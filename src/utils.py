import torch
import numpy as np
from PIL import Image
from torchvision import transforms


def weights_init(m):
    """
    Function to initialize the models weights. Follows DCGAN for conv
    and batch norm initialization values.
    """

    classname = m.__class__.__name__
    if classname.find('Linear') != -1:
        torch.nn.init.xavier_uniform_(m.weight.data)
        m.bias.data.fill_(0.01)
    if classname.find('Conv') != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0)


def evaluate_iou(generated_a, gt_a, device='cpu'):
    """
    Function used for evaluation. Compares generated and ground truth bounding
    boxes with recall. Counts a hit if the gt box has a IoU of at least 0.5
    with at least a generated box.
    """
    if gt_a.shape[0] == 0:
        return -1
    unitary_bbox = torch.tensor(
        [[0, 0, 1, 1],
         [0, 1, 0, 1],
         [1, 1, 1, 1]],
        device=device, dtype=torch.float)
    generated_a = generated_a.view(-1, 2, 3)  # nbbox x 2 x 3
    gt_a = gt_a.view(-1, 2, 3)  # ninstances x 2 x 3
    homogen = torch.tensor([0, 0, 1],
                           device=device,
                           dtype=torch.float).view(1, 3)  # 1 x 3
    homogen_gen_cat = homogen.unsqueeze(0).repeat(
        generated_a.shape[0], 1, 1)  # nbbox x 1 x 3
    homogen_gt_cat = homogen.unsqueeze(0).repeat(
        gt_a.shape[0], 1, 1)  # ninstances x 1 x 3

    generated_a = torch.cat((generated_a, homogen_gen_cat), 1)  # nbbox x 3 x 3
    gt_a = torch.cat((gt_a, homogen_gt_cat), 1)  # ninstances x 3 x 3

    generated_bbox = torch.matmul(generated_a, unitary_bbox)  # nbbox x 3 x 4
    gt_bbox = torch.matmul(gt_a, unitary_bbox)  # ninstances x 3 x 4

    percentage = 0.5
    true_positive = 0

    for i in range(gt_a.shape[0]):
        row_max = torch.max(gt_bbox[i, 0])  # rowmax
        col_max = torch.max(gt_bbox[i, 1])  # colmax
        row_min = torch.min(gt_bbox[i, 0])  # rowmin
        col_min = torch.min(gt_bbox[i, 1])  # colmin

        gt_area = (row_max - row_min) * (col_max - col_min)
        for j in range(generated_a.shape[0]):
            gen_row_max = torch.max(generated_bbox[j, 0])  # rowmax
            gen_col_max = torch.max(generated_bbox[j, 1])  # colmax
            gen_row_min = torch.min(generated_bbox[j, 0])  # rowmin
            gen_col_min = torch.min(generated_bbox[j, 1])  # colmin
            gen_area = ((gen_row_max - gen_row_min) *
                        (gen_col_max - gen_col_min))

            overlap_row_min = torch.max(row_min, gen_row_min)
            overlap_row_max = torch.min(row_max, gen_row_max)
            overlap_col_min = torch.max(col_min, gen_col_min)
            overlap_col_max = torch.min(col_max, gen_col_max)

            if ((overlap_row_max > overlap_row_min) and (
                    overlap_col_max > overlap_col_min)):
                overlap_area = ((overlap_row_max - overlap_row_min)
                                * (overlap_col_max - overlap_col_min))
                union = gt_area + gen_area - overlap_area
                iou = overlap_area / union
                if iou >= percentage:
                    true_positive += 1
                    break
    recall = true_positive / gt_a.shape[0]
    return recall

def evaluate_max_iou(generated_a, gt_a, device='cpu'):
    """
    Function used for evaluation. Computes max IoU with a generated box for
    every grounf truth box.
    """

    if gt_a.shape[0] == 0:
        return -1
    unitary_bbox = torch.tensor(
        [[0, 0, 1, 1],
         [0, 1, 0, 1],
         [1, 1, 1, 1]],
        device=device, dtype=torch.float)
    generated_a = generated_a.view(-1, 2, 3)  # nbbox x 2 x 3
    gt_a = gt_a.view(-1, 2, 3)  # ninstances x 2 x 3
    homogen = torch.tensor([0, 0, 1],
                           device=device,
                           dtype=torch.float).view(1, 3)  # 1 x 3
    homogen_gen_cat = homogen.unsqueeze(0).repeat(
        generated_a.shape[0], 1, 1)  # nbbox x 1 x 3
    homogen_gt_cat = homogen.unsqueeze(0).repeat(
        gt_a.shape[0], 1, 1)  # ninstances x 1 x 3

    generated_a = torch.cat((generated_a, homogen_gen_cat), 1)  # nbbox x 3 x 3
    gt_a = torch.cat((gt_a, homogen_gt_cat), 1)  # ninstances x 3 x 3

    generated_bbox = torch.matmul(generated_a, unitary_bbox)  # nbbox x 3 x 4
    gt_bbox = torch.matmul(gt_a, unitary_bbox)  # ninstances x 3 x 4

    max_iou = torch.zeros(gt_a.shape[0]).to(device)

    for i in range(gt_a.shape[0]):
        row_max = torch.max(gt_bbox[i, 0])  # rowmax
        col_max = torch.max(gt_bbox[i, 1])  # colmax
        row_min = torch.min(gt_bbox[i, 0])  # rowmin
        col_min = torch.min(gt_bbox[i, 1])  # colmin

        gt_area = (row_max - row_min) * (col_max - col_min)
        for j in range(generated_a.shape[0]):
            gen_row_max = torch.max(generated_bbox[j, 0])  # rowmax
            gen_col_max = torch.max(generated_bbox[j, 1])  # colmax
            gen_row_min = torch.min(generated_bbox[j, 0])  # rowmin
            gen_col_min = torch.min(generated_bbox[j, 1])  # colmin
            gen_area = ((gen_row_max - gen_row_min) *
                        (gen_col_max - gen_col_min))

            overlap_row_min = torch.max(row_min, gen_row_min)
            overlap_row_max = torch.min(row_max, gen_row_max)
            overlap_col_min = torch.max(col_min, gen_col_min)
            overlap_col_max = torch.min(col_max, gen_col_max)

            if ((overlap_row_max > overlap_row_min) and (
                    overlap_col_max > overlap_col_min)):
                overlap_area = ((overlap_row_max - overlap_row_min)
                                * (overlap_col_max - overlap_col_min))
                union = gt_area + gen_area - overlap_area
                iou = overlap_area / union
                if iou >= max_iou[i]:
                    max_iou[i] = iou
    mean_iou = torch.mean(max_iou)
    return mean_iou.item()


def evaluate(generated_a, gt_a, device='cpu'):
    """
    Function used for evaluation. Compares generated and ground truth bounding
    boxes with recall. Counts a hit if the gt box is covered by at least a
    generated box by 50% or more.
    """

    if gt_a.shape[0] == 0:
        return -1
    unitary_bbox = torch.tensor(
        [[0, 0, 1, 1],
         [0, 1, 0, 1],
         [1, 1, 1, 1]],
        device=device, dtype=torch.float)
    generated_a = generated_a.view(-1, 2, 3)  # nbbox x 2 x 3
    gt_a = gt_a.view(-1, 2, 3)  # ninstances x 2 x 3
    homogen = torch.tensor([0, 0, 1],
                           device=device,
                           dtype=torch.float).view(1, 3)  # 1 x 3
    homogen_gen_cat = homogen.unsqueeze(0).repeat(
        generated_a.shape[0], 1, 1)  # nbbox x 1 x 3
    homogen_gt_cat = homogen.unsqueeze(0).repeat(
        gt_a.shape[0], 1, 1)  # ninstances x 1 x 3

    generated_a = torch.cat((generated_a, homogen_gen_cat), 1)  # nbbox x 3 x 3
    gt_a = torch.cat((gt_a, homogen_gt_cat), 1)  # ninstances x 3 x 3

    generated_bbox = torch.matmul(generated_a, unitary_bbox)  # nbbox x 3 x 4
    gt_bbox = torch.matmul(gt_a, unitary_bbox)  # ninstances x 3 x 4

    percentage = 0.5
    true_positive = 0

    for i in range(gt_a.shape[0]):
        row_max = torch.max(gt_bbox[i, 0])  # rowmax
        col_max = torch.max(gt_bbox[i, 1])  # colmax
        row_min = torch.min(gt_bbox[i, 0])  # rowmin
        col_min = torch.min(gt_bbox[i, 1])  # colmin

        gt_area = (row_max - row_min) * (col_max - col_min)
        for j in range(generated_a.shape[0]):
            gen_row_max = torch.max(generated_bbox[j, 0])  # rowmax
            gen_col_max = torch.max(generated_bbox[j, 1])  # colmax
            gen_row_min = torch.min(generated_bbox[j, 0])  # rowmin
            gen_col_min = torch.min(generated_bbox[j, 1])  # colmin

            overlap_row_min = torch.max(row_min, gen_row_min)
            overlap_row_max = torch.min(row_max, gen_row_max)
            overlap_col_min = torch.max(col_min, gen_col_min)
            overlap_col_max = torch.min(col_max, gen_col_max)

            if ((overlap_row_max > overlap_row_min) and (
                    overlap_col_max > overlap_col_min)):
                overlap_area = ((overlap_row_max - overlap_row_min)
                                * (overlap_col_max - overlap_col_min))
                if overlap_area >= gt_area * percentage:
                    true_positive += 1
                    break
    recall = true_positive / gt_a.shape[0]
    return recall


def create_img_and_bbox(img_path, a, device='cpu',
                        label='person', test_phase=False, method='bbox'):
    """
    Function for creating rgb images with inserted bounding boxes or heatmaps.

    Parameters
    ----------
    img_path: str
        path of the rgb image to be read
    a: tensor of shape (num_bboxes, 6)
        the num_bboxes transformations generated for this image. To be
        converted in bounding boxes and then inserted
    test_phase: bool
        in test phase we must insert the generated boxes directly in then rgb
        image.
        In train phase, we just return the image read and the boxes, they will
        be inserted by tensorboard.
    method: str
        If method is 'bbox', it means that we insert the bounding boxes with
        just opaque borders, not as filled rectangles.
        If method is 'heatmap', we the boxes will be inserted as filled
        rectangles with transparency, to create a sort of heatmap.
    """

    # Read and resize image by scale factor. Then convert to torch.tensor
    scale_factor = 4
    img = transforms.Resize((1024 // scale_factor, 2048 // scale_factor),
                            interpolation=Image.NEAREST)(Image.open(img_path))
    img = torch.tensor(np.array(img), dtype=torch.uint8, device=device)

    unitary_bbox = torch.tensor(
        [[0, 0, 1, 1],
         [0, 1, 0, 1],
         [1, 1, 1, 1]],
        device=device, dtype=torch.float)
    a = a.view(-1, 2, 3)  # num_bboxes x 2 x 3
    homogen = torch.tensor([0, 0, 1],
                           device=device,
                           dtype=torch.float).view(1, 3)  # 1 x 3
    homogen_cat = homogen.unsqueeze(0).repeat(
        a.shape[0], 1, 1)  # num_bboxes x 1 x 3
    a = torch.cat((a, homogen_cat), 1)  # num_bboxes x 3 x 3
    # Transform the unitary boxes with a to obtain the final boxes coordinates.
    bbox = torch.matmul(a, unitary_bbox)  # num_bboxes x 3 x 4

    # Transform the boxes in the format expected by tensorboard.
    bboxes = torch.zeros(a.shape[0], 4, device=device)
    for i in range(a.shape[0]):
        row_max = torch.max(bbox[i, 0])  # rowmax
        col_max = torch.max(bbox[i, 1])  # colmax
        row_min = torch.min(bbox[i, 0])  # rowmin
        col_min = torch.min(bbox[i, 1])  # colmin
        tb_bbox = torch.stack((col_min, row_min, col_max, row_max))
        bboxes[i] = tb_bbox

    # Bounding boxes are generated as normalized coordinates in [0,1].
    # De-normalize them by multiplying by 1024 or 2048, then rescale by the
    # chosen scale factor.
    bboxes[:, [0, 2]] *= 2048 // scale_factor
    bboxes[:, [1, 3]] *= 1024 // scale_factor

    if not test_phase:
        return img, bboxes
    else:
        if method not in ['bbox', 'heatmap']:
            print("Uknown method. Use 'bbox' or 'heatmap'.")
            import sys
            sys.exit(1)

        if method == 'bbox':
            img = _make_bbox(img, bboxes, label=label, device=device)
        elif method == 'heatmap':
            img = _make_heatmap(img, bboxes, label=label, device=device)

        return img


def _make_heatmap(img, bboxes, label='person', device='cpu'):
    alpha = 0.1
    img = img.float() / 255
    h, w, c = img.shape
    if label == 'person':
        color = torch.tensor([1., 0., 0.], device=device)
    elif label == 'car':
        color = torch.tensor([0., 0., 1.], device=device)
    for i in range(bboxes.shape[0]):
        col_min, row_min, col_max, row_max = bboxes[i].int()
        out_rgb = (color * alpha
                   + img[row_min:row_max+1,
                         col_min:col_max+1, :3] * (1-alpha))
        img[row_min:row_max+1, col_min:col_max+1, :3] = out_rgb

    img = (img * 255).byte()
    return img


def _make_bbox(img, bboxes, b=2, label='person', device='cpu'):

    outer_mask = torch.zeros((img.shape[0], img.shape[1]),
                             device=device, dtype=torch.uint8)
    inner_mask = torch.zeros((img.shape[0], img.shape[1]),
                             device=device, dtype=torch.uint8)
    if label == 'person':
        color = torch.tensor([255., 0., 0.], device=device, dtype=torch.uint8)
    elif label == 'car':
        color = torch.tensor([0., 0., 255.], device=device, dtype=torch.uint8)

    for i in range(bboxes.shape[0]):
        col_min, row_min, col_max, row_max = bboxes[i].int()
        outer_mask[row_min-b:row_max+1+b, col_min-b:col_max+1+b].fill_(1)
        inner_mask[row_min:row_max+1, col_min:col_max+1].fill_(1)
        mask = outer_mask - inner_mask
        img[mask, :] = color
        outer_mask.fill_(0)
        inner_mask.fill_(0)

    return img


def print_losses(losses):
    """
    Function to print all the lossses during training.
    """

    print('adv_layout_loss: {:.3f} / {:.3f} | {:.3f} / {:.3f} (d/g)'.format(
        losses['train']['d_adv_layout_loss'],
        losses['train']['g_adv_layout_loss'],
        losses['validation']['d_adv_layout_loss'],
        losses['validation']['g_adv_layout_loss'],
    ))

    print('recon_loss:\t {:.3f} | {:.3f}'.format(
        losses['train']['recon_loss'],
        losses['validation']['recon_loss']
    ))

    print('vae_adv_loss:\t {:.3f} | {:.3f}'.format(
        losses['train']['vae_adv_loss'],
        losses['validation']['vae_adv_loss']
    ))

    print('l1_a:\t\t {:.3f} | {:.3f}'.format(
        losses['train']['l1_a'],
        losses['validation']['l1_a']
    ))

    print('kl_div:\t\t {:.3f} | {:.3f}'.format(
        losses['train']['kl_div'],
        losses['validation']['kl_div']
    ))

    print('adv_sup_l_loss:\t {:.3f} / {:.3f} | {:.3f} / {:.3f} (d/g)'.format(
        losses['train']['d_adv_sup_layout_loss'],
        losses['train']['g_adv_sup_layout_loss'],
        losses['validation']['d_adv_sup_layout_loss'],
        losses['validation']['g_adv_sup_layout_loss']
    ))

    print('final_g_loss:\t {:.3f} | {:.3f}'.format(
        losses['train']['final_g_loss'],
        losses['validation']['final_g_loss']
    ))

    print('D(x): {:.3f} / {:.3f} | {:.3f} / {:.3f} (layout/affine)'.format(
        losses['train']['d_boxlayout_output_real'],
        losses['train']['d_affine_output_real'],
        losses['validation']['d_boxlayout_output_real'],
        losses['validation']['d_affine_output_real'],
    ))

    print('D(G(z)): {:.3f} / {:.3f} | {:.3f} / {:.3f} (layout/affine)'.format(
        losses['train']['d_boxlayout_output_fake'],
        losses['train']['d_affine_output_fake'],
        losses['validation']['d_boxlayout_output_fake'],
        losses['validation']['d_affine_output_fake'],
    ))


def save_losses(losses, writer, epoch):
    """
    Function to add losses values to tensorbard.
    """

    writer.add_scalars(
        'losses/adv_layout_loss',
        {"train/discriminator": losses['train']['d_adv_layout_loss'],
         "train/generator": losses['train']['g_adv_layout_loss'],
         "val/discriminator": losses['validation']['d_adv_layout_loss'],
         "val/generator": losses['validation']['g_adv_layout_loss']
         },
        global_step=epoch
    )

    writer.add_scalars(
        'losses/recon_loss',
        {"train": losses['train']['recon_loss'],
         "val": losses['validation']['recon_loss']
         },
        global_step=epoch
    )

    writer.add_scalars(
        'losses/vae_adv_loss',
        {"train": losses['train']['vae_adv_loss'],
         "val": losses['validation']['vae_adv_loss']
         },
        global_step=epoch
    )

    writer.add_scalars(
        'losses/l1_a',
        {"train": losses['train']['l1_a'],
         "val": losses['validation']['l1_a']
         },
        global_step=epoch
    )

    writer.add_scalars(
        'losses/kl_div',
        {"train": losses['train']['kl_div'],
         "val": losses['validation']['kl_div']
         },
        global_step=epoch
    )

    writer.add_scalars(
        'losses/adv_sup_layout_loss',
        {"train/discriminator": losses['train']['d_adv_sup_layout_loss'],
         "train/generator": losses['train']['g_adv_sup_layout_loss'],
         "val/discriminator": losses['validation']['d_adv_sup_layout_loss'],
         "val/generator": losses['validation']['g_adv_sup_layout_loss']
         },
        global_step=epoch
    )

    writer.add_scalars(
        'losses/final_g_loss',
        {"train": losses['train']['final_g_loss'],
         "val": losses['validation']['final_g_loss']
         },
        global_step=epoch
    )

    writer.add_scalars(
        'outputs/real',
        {"train/layout": losses['train']['d_boxlayout_output_real'],
         "train/affine": losses['train']['d_affine_output_real'],
         "val/layout": losses['validation']['d_boxlayout_output_real'],
         "val/affine": losses['validation']['d_affine_output_real']
         },
        global_step=epoch
    )

    writer.add_scalars(
        'outputs/fake',
        {"train/layout": losses['train']['d_boxlayout_output_fake'],
         "train/affine": losses['train']['d_affine_output_fake'],
         "val/layout": losses['validation']['d_boxlayout_output_fake'],
         "val/affine": losses['validation']['d_affine_output_fake']
         },
        global_step=epoch
    )


def print_and_save_losses(losses, writer, epoch):
    for phase in ['train', 'validation']:
        for k, v in losses[phase].items():
            losses[phase][k] = torch.tensor(v).mean().item()

    print_losses(losses)
    save_losses(losses, writer, epoch)


if __name__ == "__main__":
    """JUSt FOR TESTING. DO NOT USE."""
    gt_a = [
        [100, 0, 100, 0, 50, 50],
        [100, 0, 100, 0, 50, 150]
    ]
    gt_a = torch.tensor(gt_a).float() / 300
    generated_a = [
        [100, 0, 90, 0, 50, 45],
        [50, 0, 0, 0, 50, 75],
        [100, 0, 190, 0, 50, 200],
        [150, 0, 90, 0, 80, 40]
    ]
    generated_a = torch.tensor(generated_a).float() / 300

    recall = evaluate(generated_a, gt_a)
    print(recall)
    max_iou = evaluate_max_iou(generated_a, gt_a)
    print(max_iou)
    iou = evaluate_iou(generated_a, gt_a)
    print(iou)

import os
import pickle

import numpy as np
import torch
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms

"""
TODO: Lots of duplicated lines. Generalize with a superclass?
"""

np.random.seed(9999)

class DataReader(Dataset):
    """
    Data reader for unsupervised path, does not care if an image does not
    contain instances.

    On inizialization:
        - initialize the rescale function
        - create a list of all the paths of the images to be read
        - open the edges dict and save it in an field.
    """

    def __init__(self, root_dir, path_edges,
                 scale_factor=1, transform=None):
        self.class_num = 35
        self.h = 1024
        self.w = 2048
        self.scale_factor = scale_factor
        if self.scale_factor > 1:
            h_rescaled = self.h // self.scale_factor
            w_rescaled = self.w // self.scale_factor
            self.rescale = transforms.Resize((h_rescaled,
                                              w_rescaled),
                                             interpolation=Image.NEAREST)
        self.label_ext = "_labelIds.png"
        self.root_dir = root_dir
        self.transforms = transform
        self.img_paths = []
        for _dir in os.listdir(self.root_dir):
            for _file in os.listdir(os.path.join(self.root_dir, _dir)):
                self.img_paths.append(
                    os.path.join(self.root_dir, _dir, _file.rsplit("_", 1)[0]))
        self.img_paths = list(np.unique(self.img_paths))
        with open(path_edges, 'rb') as f:
            self.edges_dict = pickle.load(f)

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, idx):
        """
        Lazy load of images. When invoked, reads the idx-th image from disk,
        rescales it, compute onehot encoding, concatenate it with the edges
        and finally returns the concatenated tensor.
        """
        base_path = self.img_paths[idx]
        key = base_path.split("/")[-1]

        edges_img = torch.tensor(self.edges_dict[key], dtype=torch.float)
        label_path = base_path + self.label_ext
        label_img = Image.open(label_path)

        if self.transforms:
            label_img = self.transforms(label_img)
            to_tensor = transforms.ToTensor()
            label_img = to_tensor(label_img)

        if self.scale_factor > 1:
            label_img = self.rescale(label_img)
            label_img = torch.tensor(np.array(label_img),
                                     dtype=torch.long).unsqueeze(0)

        label_one_hot = torch.zeros(self.class_num, 128, 256)
        sample = label_one_hot.scatter_(0, label_img, 1)
        sample = torch.cat((sample, edges_img.unsqueeze(0)))
        return sample


class EvaluationDataReader(object):
    """
    Similar to DataReader, but also reads the bounding box dictionary and
    returns all the ground truth bounding boxes of each image.

    Returns onehot encodings of the cleared (static only) images.
    """
    def __init__(self, clear_dir, path_bbox,
                 path_edges, scale_factor=1, transform=None):
        super(EvaluationDataReader, self).__init__()
        self.class_num = 35
        self.h = 1024
        self.w = 2048
        self.scale_factor = scale_factor
        self.clear_dir = clear_dir
        self.transforms = transform
        if self.scale_factor > 1:
            h_rescaled = self.h // self.scale_factor
            w_rescaled = self.h // self.scale_factor
            self.rescale = transforms.Resize((h_rescaled,
                                              w_rescaled),
                                             interpolation=Image.NEAREST)
        self.gt_ext = "_labelIds.png"
        self.clean_ext = self.gt_ext

        self.keys = []
        for _dir in os.listdir(self.clear_dir):
            for _file in os.listdir(os.path.join(self.clear_dir, _dir)):
                self.keys.append(os.path.join(_dir, _file.rsplit("_", 1)[0]))

        with open(path_bbox, 'rb') as f:
            self.a_dict = pickle.load(f)
        with open(path_edges, 'rb') as f:
            self.edges_dict = pickle.load(f)

    def __len__(self):
        return len(self.keys)

    def __getitem__(self, idx):
        idx = idx % len(self.keys)

        base_img = self.keys[idx]
        clear_img_path = os.path.join(self.clear_dir, base_img + self.clean_ext)
        clear_img = Image.open(clear_img_path)

        key = base_img.split("/")[-1]
        a_list = self.a_dict[key]
        a_list = torch.tensor(a_list, dtype=torch.float)

        if self.transforms:
            clear_img = self.transforms(clear_img)
            to_tensor = transforms.ToTensor()
            clear_img = to_tensor(clear_img)

        edges_img = torch.tensor(self.edges_dict[key], dtype=torch.float)

        clear_img = torch.tensor(np.array(clear_img),
                                 dtype=torch.long).unsqueeze(0)

        label_one_hot = torch.zeros(self.class_num, 128, 256)
        sample = label_one_hot.scatter_(0, clear_img, 1)
        sample = torch.cat((sample, edges_img.unsqueeze(0)))

        return sample, a_list


class DataReaderBBox(Dataset):
    """
    Similar to DataReader but continues looping until it finds an image with
    at least an instance.

    Returns also one of the instances, chosen to be the biggest or selected
    randomly.
    """
    def __init__(self, root_dir, path_bbox, path_edges,
                 scale_factor=1, transform=None):
        self.class_num = 35
        self.h = 1024
        self.w = 2048
        self.scale_factor = scale_factor
        if self.scale_factor > 1:
            h_rescaled = self.h // self.scale_factor
            w_rescaled = self.w // self.scale_factor
            self.rescale = transforms.Resize((h_rescaled,
                                              w_rescaled),
                                             interpolation=Image.NEAREST)
        self.label_ext = "_labelIds.png"
        self.rgb_ext = "_leftImg8bit.png"
        self.root_dir = root_dir
        self.transforms = transform
        self.img_paths = []
        for _dir in os.listdir(self.root_dir):
            for _file in os.listdir(os.path.join(self.root_dir, _dir)):
                self.img_paths.append(os.path.join(self.root_dir,
                                                   _dir,
                                                   _file.rsplit("_", 1)[0]))
        self.img_paths = list(np.unique(self.img_paths))
        with open(path_bbox, 'rb') as f:
            self.a_dict = pickle.load(f)
        with open(path_edges, 'rb') as f:
            self.edges_dict = pickle.load(f)

    def __len__(self):
        return len(self.img_paths)

    def get_larger(self, a_list):
        max_area = -1
        larger_a = None
        for a in a_list:
            if a[0] * a[4] > max_area:
                max_area = a[0] * a[4]
                larger_a = a
        return larger_a

    def __getitem__(self, idx):
        idx = idx % len(self.img_paths)
        base_path = self.img_paths[idx]
        key = base_path.split("/")[-1]
        a_list = self.a_dict[key]

        # Loops while it finds an empty list of instances
        while not a_list:
            idx = (idx + 1) % len(self.img_paths)
            base_path = self.img_paths[idx]
            key = base_path.split("/")[-1]
            a_list = self.a_dict[key]

        edges_img = torch.tensor(self.edges_dict[key], dtype=torch.float)
        label_path = base_path + self.label_ext
        label_img = Image.open(label_path)
        a = self.get_larger(a_list)

        if self.transforms:
            label_img = self.transforms(label_img)
            to_tensor = transforms.ToTensor()
            label_img = to_tensor(label_img)

        if self.scale_factor > 1:
            label_img = self.rescale(label_img)
            label_img = torch.tensor(np.array(label_img),
                                     dtype=torch.long).unsqueeze(0)

        a = torch.tensor(a, dtype=torch.float)
        label_one_hot = torch.zeros(self.class_num, 128, 256)
        sample = label_one_hot.scatter_(0, label_img, 1)
        sample = torch.cat((sample, edges_img.unsqueeze(0)))

        rgb_path = base_path.rsplit('_', 1)[0].replace('gtFine', 'leftImg8bit')
        rgb_path = rgb_path + self.rgb_ext

        return sample, a, rgb_path


if __name__ == "__main__":
    """
    JUST FOR TESTING DO NOT USE.
    """
    import matplotlib.patches as patches
    import matplotlib.pyplot as plt
    from PIL import ImageDraw

    def bbox_to_points(bbox):
        bbox = bbox.T
        stacked = np.column_stack((bbox[:, 0], bbox[:, 1]))
        tl, tr, bl, br = stacked
        return tl, tr, bl, br

    # def add_bbox_to_img(self, img, bbox, line_width=1):
    #     """
    #     img: tensor of shape (batch_size, 3, w, h)
    #     """
    #     color = torch.tensor([255, 0, 0])
    #     bbox = torch.transpose(bbox, 1, 2)  # 64 x 3 x 4
    #     bbox = bbox[:, :, 0:2]  # 64 x 4 x 2
    #     for b in range(bbox.shape[0]):
    #         tl, tr, bl, br = bbox[b].int()
    #         r = torch.full((), color[0])
    #         g = torch.full((), color[1])
    #         b = torch.full((), color[2])
    #         img[b, :, tl[0]:bl[0], tl[1]:tr[1]] =
    #         return img

    data_transform = transforms.Compose(
        [transforms.Resize((128, 256), interpolation=Image.NEAREST)])
    root = "./cityscapes_dataset/gtFine/train"
    path_bbox = './cityscapes_dataset/bbox.pkl'
    train_edges_path = "./cityscapes_dataset/train_edges.pkl"
    dset = DataReaderBBox(root, path_bbox, train_edges_path, scale_factor=8)
    sample, a, rgb_path = dset[0]

    # img =
    # draw = ImageDraw.Draw(img)
    # bbox = [0,0 , 0,50, 50,50, 50,0]
    # draw.rectangle([(200, 100), (550, 250)], outline='red')
    # plt.imshow(img)
    # plt.show()

    # im = np.array(Image.open(rgb_path))
    #
    # # Create figure and axes
    # fig,ax = plt.subplots(1)
    #
    # # Display the image
    # ax.imshow(im)
    #
    # # Create a Rectangle patch
    # rect = patches.Rectangle((50,100),40,30,linewidth=
    #     4,edgecolor='r',facecolor='none')
    #
    # # Add the patch to the Axes
    # ax.add_patch(rect)
    #
    # plt.show()

    # print(sample[-1, :, :].shape)
    # print(a)
    # a_concat = np.reshape(a, (2, 3))
    # b = np.array([0, 0, 1]).reshape(1, 3)
    # a_concat = np.concatenate((a_concat, b))
    # print(a_concat)
    # bbox = np.array([[0, 0, 1, 1],
    #                  [0, 1, 0, 1],
    #                  [1, 1, 1, 1]])
    # print('bbox \n', bbox)
    # bbox = np.dot(a_concat, bbox)
    # print('bbox tranformed \n', bbox)
    # top_left, top_right, bottom_left, bottom_right = bbox_to_points(bbox)
    # instance_img = sample[-1, :, :]
    # instance_img[top_left[0]:bottom_left[0],
    #   top_left[1]:top_right[1]] = 255000
    # plt.imshow(instance_img)
    # plt.show()

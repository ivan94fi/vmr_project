import os
import sys
from datetime import datetime

import torch
import torch.nn as nn
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader
from tqdm import tqdm
import imageio

import utils
from data_reader import EvaluationDataReader
from models import Generator

"""
FIXME: does not work with batch_size > 1!! => very slow!!
"""


if len(sys.argv) < 5:
    print("Usage: python test.py device label model epoch")
    sys.exit(1)

torch.manual_seed(9999)
device = torch.device(sys.argv[1])
label = sys.argv[2]
model_directory = sys.argv[3]  # '2019-02-20-12-05-28'
epoch = sys.argv[4]  # str(50)
model_save_path = os.path.join(
    "/equilibrium/prosperiscommegna/models/", model_directory)
generator_path = model_save_path + '/' + epoch + '_generator'
batch_size = 1  # DO NOT CHANGE (unless you find the bug)
shuffle = False
num_workers = 1

clear_dir = "./cityscapes_dataset/cityscapes_inpainting"
train_bbox_path = "./cityscapes_dataset/val_" + label + "_bbox.pkl"
train_edges_path = "./cityscapes_dataset/val_" + label + "_edges.pkl"


dataset = EvaluationDataReader(clear_dir, train_bbox_path, train_edges_path)
data_loader = DataLoader(dataset,
                         batch_size=batch_size,
                         shuffle=shuffle,
                         num_workers=num_workers)

total_iterations = len(dataset) // batch_size + 1
progress_bar = tqdm(enumerate(data_loader), total=total_iterations)

# Compute the metrics for these numbers of generated boxes.
num_bboxes_list = [10, 50, 100, 200, 400, 600, 700, 800, 900, 1000]

num_bboxes = max(num_bboxes_list)

generator = Generator().to(device)

# Load the selected model.
generator.load_state_dict(
    torch.load(generator_path, map_location=device))

recall_list = [[] for i in range(len(num_bboxes_list))]
iou_list = [[] for i in range(len(num_bboxes_list))]
max_iou_list = [[] for i in range(len(num_bboxes_list))]

# Repeat the experiments for 5 iterations, to get a mean and a variance.
iterations = 5

with torch.set_grad_enabled(False):
    for i, (x, a) in progress_bar:

        a = a.to(device)
        x = x.to(device)

        current_batch_size = x.shape[0]

        # a_hats is a tensor of shape (num_bboxes, current_batch_size, 6)
        a_hats = torch.zeros(num_bboxes,
                             current_batch_size,
                             6,
                             device=device)
        for it in range(iterations):
            res_max_iou = []
            res_iou = []
            res_recall = []
            for j in range(num_bboxes):
                z_l_val = torch.randn(current_batch_size,
                                      128,
                                      device=device)
                a_hats[j] = generator(x, z_l_val, device=device,
                                      test_mode=True)
            for j in range(current_batch_size):
                for idx, k in enumerate(num_bboxes_list):
                    current_recall = utils.evaluate(a_hats[:k, j, :],
                                                    a[j], device=device)
                    if current_recall >= 0:
                        recall_list[idx].append(current_recall)

                    current_max_iou = utils.evaluate_max_iou(
                        a_hats[:k, j, :],
                        a[j], device=device)
                    if current_max_iou >= 0:
                        max_iou_list[idx].append(current_max_iou)

                    current_iou = utils.evaluate_iou(a_hats[:k, j, :],
                                                     a[j], device=device)
                    if current_iou >= 0:
                        iou_list[idx].append(current_iou)


middle_iou = []
middle_max_iou = []
middle_recall = []

recall_array = torch.tensor(recall_list)
iou_array = torch.tensor(iou_list)
max_iou_array = torch.tensor(max_iou_list)

batch_step = int(len(iou_list[0])/iterations)

print(recall_array.shape)
print(batch_step)

for it in range(iterations):
    middle_recall.append(torch.mean(
        recall_array[:, it * batch_step:(it + 1) * batch_step - 1], dim=1))
    middle_max_iou.append(torch.mean(
        max_iou_array[:, it * batch_step:(it + 1) * batch_step - 1], dim=1))
    middle_iou.append(torch.mean(
        iou_array[:, it * batch_step:(it + 1) * batch_step - 1], dim=1))


mean_recall = torch.mean(torch.stack(middle_recall), dim=0)
var_recall = torch.var(torch.stack(middle_recall), dim=0)

mean_iou = torch.mean(torch.stack(middle_iou), dim=0)
var_iou = torch.var(torch.stack(middle_iou), dim=0)

mean_max_iou = torch.mean(torch.stack(middle_max_iou), dim=0)
var_max_iou = torch.var(torch.stack(middle_max_iou), dim=0)

print('recall: \n mean: {} \n var: {}'.format(mean_recall, var_recall))
print('iou: \n mean: {} \n var: {}'.format(mean_iou, var_iou))
print('max_iou: \n mean: {} \n var: {}'.format(mean_max_iou, var_max_iou))

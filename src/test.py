import os
import sys
from datetime import datetime

import torch
import torch.nn as nn
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader
from tqdm import tqdm
import imageio

import utils
from data_reader import DataReader, DataReaderBBox
from models import AffineDiscriminator, BoxLayoutDiscriminator, Generator

if len(sys.argv) < 6:
    print("Usage: python test.py device label model epoch num_bboxes")
    sys.exit(1)

torch.manual_seed(9999)
device = torch.device(sys.argv[1])
label = sys.argv[2]
model_directory = sys.argv[3]  # '2019-02-20-12-05-28'
model_save_path = os.path.join(
    "/equilibrium/prosperiscommegna/models/", model_directory)

test_root = "./cityscapes_dataset/gtFine/val"
test_bbox_path = "./cityscapes_dataset/val_" + label + "_bbox.pkl"
test_edges_path = "./cityscapes_dataset/val_" + label + "_edges.pkl"
epoch = sys.argv[4]  # str(50)
scale_factor = 8
batch_size = 8
shuffle = False
num_workers = 4
generator_path = model_save_path + '/' + epoch + '_generator'
num_bboxes = int(sys.argv[5])
resized_h = 128
save_on_disk = True
img_save_dir = '/equilibrium/prosperiscommegna/test_images'

if not save_on_disk:
    current_time = datetime.now().strftime('%b%d_%H-%M-%S')
    log_base = '/equilibrium/prosperiscommegna/runs/test'
    log_dir = os.path.join(log_base, current_time)

    writer = SummaryWriter(log_dir=log_dir)

if not os.path.isdir(img_save_dir):
    os.makedirs(img_save_dir)

test_bbox_dataset = DataReaderBBox(test_root, test_bbox_path,
                                   test_edges_path,
                                   scale_factor=scale_factor)
test_bbox_loader = DataLoader(test_bbox_dataset,
                              batch_size=batch_size,
                              shuffle=shuffle,
                              num_workers=num_workers)

test_dataset = DataReader(test_root, test_edges_path,
                          scale_factor=scale_factor)
test_loader = DataLoader(test_dataset,
                         batch_size=batch_size,
                         shuffle=shuffle,
                         num_workers=num_workers)

generator = Generator().to(device)

generator.load_state_dict(
    torch.load(generator_path, map_location=device))

generator.eval()

# adversarial_loss = nn.BCELoss()
# l1_loss = nn.L1Loss(reduction='mean')

total_iterations = len(test_dataset) // batch_size + 1
progress_bar = tqdm(
    enumerate(zip(test_bbox_loader, test_loader)),
    total=total_iterations)

previous_path = None

with torch.set_grad_enabled(False):
    for i, (bbox_data, x) in progress_bar:

        bbox_x, a, rgb_paths = bbox_data
        bbox_x = bbox_x.to(device)
        a = a.to(device)
        x = x.to(device)

        # if rgb_paths[0] == previous_path:
        #     continue
        # previous_path = rgb_paths[0]
        current_batch_size = x.shape[0]

        # method = 'bbox'
        method = 'heatmap'

        # a_hats is a tensor of shape (num_bboxes, current_batch_size, 6)
        a_hats = torch.zeros(num_bboxes,
                             current_batch_size,
                             6,
                             device=device)

        # Forward pass num_bboxes times to otain num_bboxes outputs for each
        # image in the current batch. Put the results in a_hats
        for j in range(num_bboxes):
            z_l_val = torch.randn(current_batch_size,
                                  resized_h,
                                  device=device)
            a_hats[j] = generator(x, z_l_val, a, bbox_x, device, test_mode=True)

        # a_hats[:, j, :] contains num_bboxes a_hats for each image.
        for j in range(current_batch_size):
            name, ext = rgb_paths[j].split('/')[-1].split('.')
            img_name = name + '.' + label + '.' + ext
            img_name = 'test_' + img_name

            img = utils.create_img_and_bbox(
                rgb_paths[j], a_hats[:, j, :], label=label,
                test_phase=True, method=method, device=device)

            progress_bar.write(img_name)
            if save_on_disk:
                imageio.imwrite(os.path.join(img_save_dir, img_name),
                                img.cpu().numpy())
            else:
                writer.add_image(img_name, img,
                                 global_step=epoch, dataformats="HWC")

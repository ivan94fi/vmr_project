import os
import pickle
import numpy as np
# import imageio
import json
import sys

"""
This script creates a pkl file with all the ground truth bounding boxes for
the chosen dataset split.

These bounding boxes are used by our data loaders.
"""

if len(sys.argv) < 3:
    print("Usage: create_bbox.py [train|val|test] [car|person]")
    sys.exit(1)

split = sys.argv[1]

label = sys.argv[2]  # person or car
path_save = './cityscapes_dataset/{}_{}_bbox.pkl'.format(split, label)
root_dir = './cityscapes_dataset/gtFine/{}'.format(split)
json_ext = 'polygons.json'
a_dict = {}

for _dir in os.listdir(root_dir):
    for _file in os.listdir(os.path.join(root_dir, _dir)):
        if _file.split('_')[-1] == json_ext:
            # print(_file)
            with open(os.path.join(os.path.join(root_dir, _dir, _file))) as f:
                _json = json.load(f)
            a_list = []
            for item in _json['objects']:
                if item['label'] == label:
                    poly = np.array(item['polygon'])
                    max_c, max_r = np.max(poly, axis=0)
                    min_c, min_r = np.min(poly, axis=0)
                    h = max_r - min_r
                    w = max_c - min_c
                    top_left = (min_r, min_c)
                    # print('top_left: ' + str(top_left))
                    a = [h, 0, top_left[0], 0, w, top_left[1]]
                    # end new
                    for i in range(3):
                        a[i] /= 1024
                        a[i+3] /= 2048
                    a_list.append(a)
            a_dict[_file.rsplit('_', 1)[0]] = a_list

with open(path_save, 'wb') as f:
    pickle.dump(a_dict, f)

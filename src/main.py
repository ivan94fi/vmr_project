import os
import sys
from datetime import datetime
import math
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from PIL import Image
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader
from torchvision import transforms
from tqdm import tqdm

import utils
from data_reader import DataReader, DataReaderBBox
from models import AffineDiscriminator, BoxLayoutDiscriminator, Generator

"""
Command line arguments to be specified:
    device [cuda:x | cpu]: device where to run the code (see Pytorch docs)
    label [person | car]: objects from cityscapes to utilize
    std [float]: standard deviation of added gaussian noise
"""
if len(sys.argv) < 4:
    print("Usage: python main.py device label std")
    sys.exit(1)



# Config for tensorboardX.
current_time = datetime.now().strftime('%b%d_%H-%M-%S')
log_base = '/equilibrium/prosperiscommegna/runs'
log_dir = os.path.join(log_base, current_time)
writer = SummaryWriter(log_dir=log_dir)

"""
Main program.
"""
if __name__ == "__main__":

    # ************ PARAMETERS ************ #
    torch.manual_seed(9999)
    label = sys.argv[2]
    std = sys.argv[3]

    # Dataset paths.
    train_root = "./cityscapes_dataset/gtFine/train"
    val_root = "./cityscapes_dataset/gtFine/val"
    test_root = "./cityscapes_dataset/gtFine/test"

    # Paths for box and edges extracted offline in advance.
    train_bbox_path = "./cityscapes_dataset/train_" + label + "_bbox.pkl"
    train_edges_path = "./cityscapes_dataset/train_" + label + "_edges.pkl"
    val_bbox_path = "./cityscapes_dataset/val_" + label + "_bbox.pkl"
    val_edges_path = "./cityscapes_dataset/val_" + label + "_edges.pkl"
    test_bbox_path = "./cityscapes_dataset/test_" + label + "_bbox.pkl"
    test_edges_path = "./cityscapes_dataset/test_" + label + "_edges.pkl"
    model_save_path = "/equilibrium/prosperiscommegna/models"

    epochs = 30
    batch_size = 64
    shuffle = True  # shuffle dataset
    num_workers = 4  # threads to load dataset

    # input size of the network. Original images are resized on the fly.
    resized_h = 128
    resized_w = 256

    # To be passed at the data loader to resize images correctly:
    #   (1024,2048) -> (128,256)
    scale_factor = 8

    lr = 0.00002  # learning rate
    beta1 = 0.5  # parameter of Adam optimizer

    # Label values.
    real_label = 1
    real_soft_label = 0.9
    fake_label = 0

    alpha = 0.02  # coefficient for expoential annealing of probability
    k_percent = 0.5  # % of labels to be flipped when label flipping occurs

    device = torch.device(sys.argv[1])

    # Config for model checkpointing
    datestring = datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M-%S')
    model_save_path = os.path.join(
        "/equilibrium/prosperiscommegna/models/", datestring)
    os.makedirs(model_save_path, exist_ok=True)
    # ************ END PARAMETERS ************ #

    # Data loader for supervised path: always check that an instance of the
    # selected class is present.
    train_bbox_dataset = DataReaderBBox(train_root, train_bbox_path,
                                        train_edges_path,
                                        scale_factor=scale_factor)
    train_bbox_loader = DataLoader(train_bbox_dataset,
                                   batch_size=batch_size,
                                   shuffle=shuffle,
                                   num_workers=num_workers)

    # Data loader for unsupervised_path: accepts images without an instance of
    # the selectd class.
    train_dataset = DataReader(train_root, train_edges_path,
                               scale_factor=scale_factor)
    train_loader = DataLoader(train_dataset,
                              batch_size=batch_size,
                              shuffle=shuffle,
                              num_workers=num_workers)


    # Validaiton data loaders (identical to the train loaders,
    # but load val images).
    val_bbox_dataset = DataReaderBBox(val_root,
                                      val_bbox_path,
                                      val_edges_path,
                                      scale_factor=scale_factor)
    val_bbox_loader = DataLoader(val_bbox_dataset,
                                 batch_size=batch_size,
                                 shuffle=False,
                                 num_workers=num_workers)

    val_dataset = DataReader(val_root, val_edges_path,
                             scale_factor=scale_factor)
    val_loader = DataLoader(val_dataset,
                            batch_size=batch_size,
                            shuffle=False,
                            num_workers=num_workers)


    """
    Model instantiation. Move models to device.
    """
    generator = Generator().to(device)
    affine_discriminator = AffineDiscriminator().to(device)
    boxlayout_discriminator = BoxLayoutDiscriminator(bbox_layer=True,
                                                     device=device,
                                                     label=label,
                                                     std=std).to(device)

    # Apply the weights_init function to randomly initialize all weights
    #  to mean=0, stdev=0.2.
    generator.apply(utils.weights_init)
    affine_discriminator.apply(utils.weights_init)
    boxlayout_discriminator.apply(utils.weights_init)

    # Initialize loss functions.
    adversarial_loss = nn.BCELoss()
    l1_loss = nn.L1Loss(reduction='mean')

    # Initialize optimizers.
    d_affine_optimizer = optim.Adam(
        affine_discriminator.parameters(), lr=lr, betas=(beta1, 0.999))
    d_boxlayout_optimizer = optim.Adam(
        boxlayout_discriminator.parameters(), lr=lr, betas=(beta1, 0.999))
    g_optimizer = optim.Adam(
        generator.parameters(), lr=lr, betas=(beta1, 0.999))

    ############
    # Main loop
    ############
    print("Starting main loop")


    # This dict is utilized to print the losses.
    losses = {}
    losses['train'] = {}
    losses['validation'] = {}

    # This dict is to aggregate all loaders in one place.
    data_loaders = {}
    data_loaders['train'] = (train_bbox_loader, train_loader)
    data_loaders['validation'] = (val_bbox_loader, val_loader)

    for epoch in range(epochs):
        print("epoch [{}/{}]".format(epoch, epochs))

        ################
        # Train + Validation loop
        ################

        # First do an epoch on train images, then an epoch on val images.
        for phase in ['train', 'validation']:

            print("Phase: {}".format(phase))

            # Set the right phase for models (See Pytorch docs).
            if phase == 'train':
                generator.train()
                boxlayout_discriminator.train()
                affine_discriminator.train()
            else:
                generator.eval()
                boxlayout_discriminator.eval()
                affine_discriminator.eval()

            # Initialize losses dict
            losses[phase]['g_adv_layout_loss'] = []
            losses[phase]['recon_loss'] = []
            losses[phase]['vae_adv_loss'] = []
            losses[phase]['final_g_loss'] = []
            losses[phase]['d_adv_layout_loss'] = []
            losses[phase]['d_adv_sup_layout_loss'] = []
            losses[phase]['l1_a'] = []
            losses[phase]['kl_div'] = []
            losses[phase]['g_adv_sup_layout_loss'] = []
            losses[phase]['d_boxlayout_output_real'] = []
            losses[phase]['d_affine_output_real'] = []
            losses[phase]['d_boxlayout_output_fake'] = []
            losses[phase]['d_affine_output_fake'] = []

            # During training show progress bar, during validation do not show
            # anything.
            if phase == 'train':
                dataset = train_dataset if phase == 'train' else val_dataset
                total_iterations = len(dataset) // batch_size + 1
                progress_bar = tqdm(
                    enumerate(zip(*data_loaders[phase])),
                    total=total_iterations)
            else:
                progress_bar = enumerate(zip(*data_loaders[phase]))

            # Loop through data: we need both unsupervised and supervised data.
            for i, (bbox_data, x) in progress_bar:

                # Extract data cominig from DataReaderBBox.
                bbox_x, a, rgb_paths = bbox_data
                # Transfer data on device.
                bbox_x = bbox_x.to(device)
                a = a.to(device)
                x = x.to(device)

                # In the last iteration current_batch_size < batch_size, so
                # so we get the batch size from x shape.
                current_batch_size = x.shape[0]

                # During the training phase we want to enable gradient
                # tracking, whereas in evaluation we disable them to speed up
                # the computation.
                with torch.set_grad_enabled(phase == 'train'):

                    #############
                    # Train + Validate discriminators
                    #############

                    # First zero out the gradients.
                    affine_discriminator.zero_grad()
                    boxlayout_discriminator.zero_grad()

                    # Construct the label vectors.
                    real_labels = torch.full((current_batch_size,),
                                             real_soft_label, device=device)
                    fake_labels = torch.full((current_batch_size,),
                                             fake_label, device=device)

                    real_flip_labels = torch.full((current_batch_size,),
                                                  real_soft_label,
                                                  device=device)

                    fake_flip_labels = torch.full((current_batch_size,),
                                                  fake_label, device=device)

                    # Define flip probabilty as exponentally decreasing with
                    # epochs.
                    flip_probability = math.exp(-epoch * alpha)

                    # Flip k_percent % of labels, chosen at random in the
                    # label vector
                    if torch.rand(1).item() < flip_probability:
                        perm = torch.randperm(current_batch_size)
                        k = int(current_batch_size * k_percent)
                        idx, _ = torch.sort(perm[:k])
                        fake_flip_labels[idx] = real_soft_label
                        real_flip_labels[idx] = fake_label

                    d_boxlayout_output_real = boxlayout_discriminator(
                        bbox_x, a)

                    losses[phase]['d_boxlayout_output_real'].append(
                        d_boxlayout_output_real.mean().item())

                    # Compute adversarial losses with all-real mini-batch.
                    d_adv_layout_loss_real = adversarial_loss(
                        d_boxlayout_output_real, real_flip_labels)

                    d_affine_output_real = affine_discriminator(a)
                    losses[phase]['d_affine_output_real'].append(
                        d_affine_output_real.mean().item())
                    d_adv_sup_layout_loss_real = adversarial_loss(
                        d_affine_output_real, real_labels)

                    # Draw a random vector from a normal gaussian and pass it
                    # to the generator.
                    z_l = torch.randn(current_batch_size,
                                      resized_h, device=device)
                    reconstructed, a_hat, a_tilde, z_a, mu, logvar = generator(
                        x, z_l, a, bbox_x, device)

                    # /**** Detach the output of the generator ****/
                    d_boxlayout_output_fake = boxlayout_discriminator(
                        x.detach(), a_hat.detach())
                    losses[phase]['d_boxlayout_output_fake'].append(
                        d_boxlayout_output_fake.mean().item())

                    d_affine_output_fake = affine_discriminator(
                        a_tilde.detach())
                    losses[phase]['d_affine_output_fake'].append(
                        d_affine_output_fake.mean().item())

                    # Compute adversarial losses with all-fake mini-batch.
                    d_adv_layout_loss_fake = adversarial_loss(
                        d_boxlayout_output_fake, fake_flip_labels)

                    d_adv_sup_layout_loss_fake = adversarial_loss(
                        d_affine_output_fake, fake_labels)

                    # Sum the fake and real losses to obtain the final
                    # adversarial losses.
                    d_adv_layout_loss = (d_adv_layout_loss_fake
                                         + d_adv_layout_loss_real)
                    losses[phase]['d_adv_layout_loss'].append(
                        d_adv_layout_loss.item())

                    d_adv_sup_layout_loss = (d_adv_sup_layout_loss_fake
                                             + d_adv_sup_layout_loss_real)
                    losses[phase]['d_adv_sup_layout_loss'].append(
                        d_adv_sup_layout_loss.item())

                    # Backpropagation and optimizer step
                    if phase == 'train':
                        d_adv_layout_loss.backward()
                        d_adv_sup_layout_loss.backward()
                        d_boxlayout_optimizer.step()
                        d_affine_optimizer.step()

                    ############
                    # Train + Validate generator
                    ############

                    generator.zero_grad()

                    d_boxlayout_output_fake = boxlayout_discriminator(x, a_hat)

                    d_affine_output_fake = affine_discriminator(a_tilde)

                    # !!! Generator adversarial loss with real instead of fake
                    # in not an error: see pytorch dcgan tutorial !!!
                    g_adv_layout_loss = adversarial_loss(
                        d_boxlayout_output_fake, real_labels)

                    g_adv_sup_layout_loss = adversarial_loss(
                        d_affine_output_fake, real_labels)

                    losses[phase]['g_adv_sup_layout_loss'].append(
                        g_adv_sup_layout_loss.item())

                    losses[phase]['g_adv_layout_loss'].append(
                        g_adv_layout_loss.item())

                    # Extract the two components from the reconstructed tensor.
                    x_prime = reconstructed[:, :-1]
                    z_prime = reconstructed[:, -1, :, 0]

                    # Compute L1 losses with the reconstructed tensors.
                    l1_x = l1_loss(x_prime, x)
                    l1_z = l1_loss(z_prime, z_l)
                    recon_loss = l1_x + l1_z
                    losses[phase]['recon_loss'].append(recon_loss.item())

                    # Compute vae_adv_loss = l1 on a
                    #                      + generator adversarial loss
                    #                      + kl divergence

                    l1_a = l1_loss(a, a_tilde)

                    # Computed with closed form because the two distriution
                    # are gaussian.
                    kl_div = -0.5 * torch.mean(1 + logvar - mu.pow(2) -
                                               logvar.exp())

                    vae_adv_loss = l1_a + g_adv_sup_layout_loss + kl_div

                    losses[phase]['l1_a'].append(l1_a.item())
                    losses[phase]['kl_div'].append(kl_div.item())
                    losses[phase]['vae_adv_loss'].append(
                        vae_adv_loss.item())

                    final_g_loss = (g_adv_layout_loss +
                                    recon_loss + vae_adv_loss)
                    losses[phase]['final_g_loss'].append(final_g_loss.item())

                    # Backpropagation and optimizer step for generator.
                    if phase == 'train':
                        final_g_loss.backward()
                        g_optimizer.step()

                    # In validation, send the images with generated bounding
                    # boxes to tensorboard. Do this only for two iterations,
                    # just to see some results.
                    if phase == 'validation' and i < 2:
                        num_bboxes = 5
                        a_hats = torch.zeros(num_bboxes,
                                            current_batch_size,
                                            6,
                                            device=device)
                        # Generate num_bboxes bounding boxes by forward
                        # passing num_bboxes times on generator.
                        for j in range(num_bboxes):
                            z_l_val = torch.randn(current_batch_size,
                                              resized_h, device=device)
                            _, a_hat_val, _, _, _, _ = generator(
                                x, z_l_val, a, bbox_x, device)
                            a_hats[j] = a_hat_val

                        # For each image in the mini-batch, read the rgb img
                        # and transform the bounding boxes in the format
                        # expected by tensorboard.
                        for j in range(current_batch_size):
                            img, bbox = utils.create_img_and_bbox(
                                rgb_paths[j], a_hats[:, j, :],
                                label=label, device=device)

                            # Send images and boxes to tensorboard (it will
                            # blend them automatically).
                            writer.add_image_with_boxes(
                                rgb_paths[j], img, bbox,
                                global_step=epoch, dataformats="HWC")

                    # End with set_grad_enabled

                # End train (validation) loop on data

            # End phase loop

        # Print and save losses
        utils.print_and_save_losses(losses, writer, epoch)
        print('')

        # Save models weights every 50 epochs
        if (epoch != 0 and epoch % 5 == 0) or epoch == epochs - 1:
            torch.save(generator.state_dict(),
                       os.path.join(model_save_path,
                                    '{}_generator'.format(epoch)))
            torch.save(boxlayout_discriminator.state_dict(),
                       os.path.join(model_save_path,
                                    '{}_boxlayout_discriminator'.format(
                                        epoch)))
            torch.save(affine_discriminator.state_dict(),
                       os.path.join(model_save_path,
                                    '{}_affine_discriminator'.format(epoch)))

        # End main loop

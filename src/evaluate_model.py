
"""
Do not use.
"""

# import os
# import sys
# from datetime import datetime
#
# import torch
# import torch.nn as nn
# from tensorboardX import SummaryWriter
# from torch.utils.data import DataLoader
# from tqdm import tqdm
# import imageio
#
# import utils
# from data_reader import EvaluationDataReader
# from models import Generator
#
# if len(sys.argv) < 5:
#     print("Usage: python test.py device label model epoch")
#     sys.exit(1)
#
# torch.manual_seed(9999)
# device = torch.device(sys.argv[1])
# label = sys.argv[2]
# model_directory = sys.argv[3]  # '2019-02-20-12-05-28'
# epoch = sys.argv[4]  # str(50)
# model_save_path = os.path.join(
#     "/equilibrium/prosperiscommegna/models/", model_directory)
# generator_path = model_save_path + '/' + epoch + '_generator'
# batch_size = 1
# shuffle = False
# num_workers = 1
#
# clear_dir = "./cityscapes_dataset/cityscapes_inpainting"
# train_bbox_path = "./cityscapes_dataset/val_" + label + "_bbox.pkl"
# train_edges_path = "./cityscapes_dataset/val_" + label + "_edges.pkl"
#
#
# dataset = EvaluationDataReader(clear_dir, train_bbox_path, train_edges_path)
# data_loader = DataLoader(dataset,
#                          batch_size=batch_size,
#                          shuffle=shuffle,
#                          num_workers=num_workers)
#
# total_iterations = len(dataset) // batch_size + 1
# progress_bar = tqdm(enumerate(data_loader), total=total_iterations)
#
# # num_bboxes_list = [10, 50, 100, 200, 400, 600, 700, 800, 900, 1000]
# num_bboxes_list = [1, 2, 3, 5]
# num_bboxes = max(num_bboxes_list)
#
# generator = Generator().to(device)
#
# generator.load_state_dict(
#     torch.load(generator_path, map_location=device))
#
# recall_list = [[] for i in range(len(num_bboxes_list))]
#
# with torch.set_grad_enabled(False):
#     for i, (x, a) in progress_bar:
#
#         a = a.to(device)
#         x = x.to(device)
#
#         current_batch_size = x.shape[0]
#
#         # a_hats is a tensor of shape (num_bboxes, current_batch_size, 6)
#         a_hats = torch.zeros(num_bboxes,
#                              current_batch_size,
#                              6,
#                              device=device)
#
#         for j in range(num_bboxes):
#             z_l_val = torch.randn(current_batch_size,
#                                   128,
#                                   device=device)
#             a_hats[j] = generator(x, z_l_val, a, device=device, test_mode=True)
#
#         for j in range(current_batch_size):
#             for idx, i in enumerate(num_bboxes_list):
#                 current_recall = utils.evaluate(a_hats[:i, j, :],
#                                                 a[j], device=device)
#                 if current_recall >= 0:
#                     recall_list[idx].append(current_recall)
#
#
# recall = torch.mean(torch.tensor(recall_list), dim=1)
# print(recall)

import torch
import torch.nn as nn
from torchsummary import summary

class Flatten(nn.Module):
    """Module to flatten a tensor."""

    def forward(self, x):
        return x.view(x.size()[0], -1)


class Generator(nn.Module):
    """
    Module that ecapsulates the functionalities of the generator as class
    properties.

    Attributes
    ----------
    encoder: torch.nn.Sequential
        a convolutional network that encodes x and z as the vector e
    stn: torch.nn.Sequential
        simple fully connected network representing the localization module of
        an STN.
    reconstructor: torch.nn.Sequential
        convolutional transpose network, reconstruct x and z from e
    common: torch.nn.Sequential
        part of the Variational Autoencoder
    mu: torch.nn.Linear
        part of the Variational Autoencoder
    logvar: torch.nn.Linear
        part of the Variational Autoencoder

    Methods
    -------
    unsupervised_path(x, z, device)
        performs the unsupervised path of the approach
    supervised_path( a, x_plus, device)
        performs the supervised path of the approach
    forward(x, z, a=None, x_plus=None, device='cpu', test_mode=False)
        standard override of torch.nn.Module.forward(): performs supervised
        and unsupervised path, depending whether we are in test mode or not
    """

    def __init__(self):
        super(Generator, self).__init__()

        self.nc = 37  # input number of channels
        self.depth = 64  # feature maps base depth
        self.k = 3  # kernel size
        self.s = 2  # convolution stride
        self.p = 1  # convolution padding

        self.encoder = nn.Sequential(
            # input shape: nc * h * w
            nn.Conv2d(self.nc, self.depth, self.k, self.s, self.p, bias=False),
            nn.LeakyReLU(0.2, inplace=True),

            # shape: (depth) * 64 * 128
            nn.Conv2d(self.depth, self.depth * 2, self.k,
                      self.s, self.p, bias=False),
            nn.BatchNorm2d(self.depth * 2),
            nn.LeakyReLU(0.2, inplace=True),

            # shape: (depth * 2) * 32 * 64
            nn.Conv2d(self.depth * 2, self.depth * 4,
                      self.k, self.s, self.p, bias=False),
            nn.BatchNorm2d(self.depth * 4),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv2d(self.depth * 4, self.depth * 8,
                      self.k, self.s, self.p, bias=False),
            nn.BatchNorm2d(self.depth * 8),
            nn.LeakyReLU(0.2, inplace=True),

            # shape (depth * 4) * 16 * 32
            Flatten(),
            nn.Linear((self.depth * 8) * 8 * 16, self.depth * 4),
            nn.Tanh()

            # output shape: (depth * 4) * 1 * 1
        )

        """
        stn: takes the vector e returned from encoder and learns a
             transform matrix A (linearized to a vector of 4 elements)

        NOTE: The two zeros in the affine transform are not learned,
               but inserted manually in unsupervised_path()
        """
        self.stn = nn.Sequential(
            nn.Linear((self.depth * 4), 4),
        )

        self.reconstructor = nn.Sequential(

            nn.ConvTranspose2d((self.depth * 4), int(self.depth * 3.5),
                               kernel_size=(8, 16), stride=(1, 1),
                               padding=(0, 0), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(int(self.depth * 3.5)),
            nn.ReLU(True),

            nn.ConvTranspose2d(int(self.depth * 3.5), (self.depth * 3),
                               kernel_size=(4, 4), stride=(2, 2),
                               padding=(1, 1), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(self.depth * 3),
            nn.ReLU(True),

            nn.ConvTranspose2d((self.depth * 3), (self.depth * 2),
                               kernel_size=(4, 4), stride=(2, 2),
                               padding=(1, 1), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(self.depth * 2),
            nn.ReLU(True),

            nn.ConvTranspose2d((self.depth * 2), self.depth,
                               kernel_size=(4, 4), stride=(2, 2),
                               padding=(1, 1), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(self.depth),
            nn.ReLU(True),

            nn.ConvTranspose2d(self.depth, self.nc,
                               kernel_size=(4, 4), stride=(2, 2),
                               padding=(1, 1), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(self.nc),
            nn.Sigmoid(),
        )
        self.common = nn.Sequential(
            nn.Linear(6, 64),
            nn.ReLU(True)
        )
        self.mu = nn.Linear(64, 128)
        self.logvar = nn.Linear(64, 128)

    def unsupervised_path(self, x, z, device):

        # Tile the noise vector
        z = z.view(-1, 1).repeat(1, 256).view(x.shape[0], 128, -1)
        # Concat with x
        concat = torch.cat((x, z.unsqueeze(1)), 1)

        # concat is a tensor with shape (batch_size, 36, 128, 256)

        e = self.encoder(concat)  # shape (batch_size, 256)
        e_unsqueezed = e.view(e.shape[0], e.shape[1], 1, 1)
        reconstructed = self.reconstructor(e_unsqueezed)

        a_hat = self.stn(e)  # shape (batch_size, 4)

        # Reshape and insert the zeros in the proper place.
        a_hat = torch.stack([a_hat[:, 0],
                            torch.zeros(x.shape[0]).to(device),
                            a_hat[:, 1],
                            torch.zeros(x.shape[0]).to(device),
                            a_hat[:, 2],
                            a_hat[:, 3]],
                            dim=1)

        return reconstructed, a_hat

    def supervised_path(self, a, x_plus, device):

        # Compute the distribution parameters mu and logvar with the VAE.
        common = self.common(a)
        mu, logvar = self.mu(common), self.logvar(common)
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)

        # Draw z_a from the learned distribution.
        z_a = eps.mul(std).add_(mu)

        # Tile noise vector.
        z_a_concat = z_a.view(-1, 1).repeat(
            1, 256).view(x_plus.shape[0], 128, -1).unsqueeze(1)

        # Concat noise with x.
        concat = torch.cat((x_plus, z_a_concat), 1)

        # Reshape and insert zeros.
        a_tilde = self.stn(self.encoder(concat))
        a_tilde = torch.stack([a_tilde[:, 0],
                               torch.zeros(x_plus.shape[0]).to(device),
                               a_tilde[:, 1],
                               torch.zeros(x_plus.shape[0]).to(device),
                               a_tilde[:, 2],
                               a_tilde[:, 3]],
                              dim=1)

        return a_tilde, z_a, mu, logvar

    def forward(self, x, z, a=None,
            x_plus=None, device='cpu', test_mode=False):
        reconstructed, a_hat = self.unsupervised_path(x, z, device)
        if not test_mode:
            a_tilde, z_a, mu, logvar = self.supervised_path(a, x_plus, device)
            return reconstructed, a_hat, a_tilde, z_a, mu, logvar
        else:
            return a_hat

class AffineDiscriminator(nn.Module):
    """Predicts probability that the input affine transformation parameters
       are realistic."""

    def __init__(self):
        super(AffineDiscriminator, self).__init__()
        self.main = nn.Sequential(
            nn.Linear(6, 64),
            nn.ReLU(),
            nn.Linear(64, 1),
            nn.Sigmoid()
        )

    def forward(self, a):
        return self.main(a).squeeze()

class BoxLayoutDiscriminator(nn.Module):
    """
    Defines a PatchGAN discriminator.

    source: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/models/networks.py#L532

    Methods
    -------
    add_bbox_to_input(self, x, bbox)
        add the bbox in the in the instance layer (24 or 26) of the onehot
        encoding
    add_bbox_layer_to_input(self, x, bbox)
        add the bbox in a new layer, thus extending the dimension from 36 to 37
    """

    def __init__(self, device="cuda:0", ndf=64,
                 n_layers=3, norm_layer=nn.BatchNorm2d,
                 bbox_layer=False, std=0.5, label='person'):
        """
        Construct a PatchGAN discriminator
         Original Parameters (control the network architecure): (DO NOT CHANGE)
            input_nc: int
                the number of channels in input images
            ndf: int
                the number of filters in the last conv layer
            n_layers: int
                the number of conv layers in the discriminator
            norm_layer:
                normalization layer

        Our parameters:
            bbox_layer: bool
                construct a whole new layer to insert the instance
            std: float
                standard deviation of gaussian noise to be added at instances
            label: str
                instance class to use

        """

        super(BoxLayoutDiscriminator, self).__init__()
        self.device = device
        self.bbox_layer = bbox_layer
        self.std = float(std)

        # Define the right layer for the chosen instance. This corresponds to
        # class id in cityscapes and is used to index the onehot tensor.
        if label == 'person':
            self.label_layer = 24
        elif label == 'car':
            self.label_layer = 26
        else:
            import sys
            print("Error: label must be either 'car' or 'person'. Aborting.")
            sys.exit(1)
        use_bias = False
        kw = 4
        padw = 1
        initial_depth = 37 if self.bbox_layer else 36
        sequence = [nn.Conv2d(initial_depth, ndf, kernel_size=kw,
                              stride=2, padding=padw), nn.LeakyReLU(0.2, True)]
        nf_mult = 1
        nf_mult_prev = 1
        for n in range(1, n_layers):  # gradually increase the number of filter
            nf_mult_prev = nf_mult
            nf_mult = min(2 ** n, 8)
            sequence += [
                nn.Conv2d(ndf * nf_mult_prev,
                          ndf * nf_mult,  kernel_size=kw,
                          stride=2, padding=padw, bias=use_bias),
                norm_layer(ndf * nf_mult),
                nn.LeakyReLU(0.2, True)
            ]

        nf_mult_prev = nf_mult
        nf_mult = min(2 ** n_layers, 8)
        sequence += [
            nn.Conv2d(ndf * nf_mult_prev, ndf * nf_mult,
                      kernel_size=kw, stride=1, padding=padw, bias=use_bias),
            norm_layer(ndf * nf_mult),
            nn.LeakyReLU(0.2, True)
        ]
        # output 1 channel prediction map
        sequence += [nn.Conv2d(ndf * nf_mult, 1,
                               kernel_size=kw, stride=1, padding=padw)]
        sequence += [nn.Sigmoid()]
        sequence += [nn.AvgPool2d(kernel_size=(14, 30))]
        self.model = nn.Sequential(*sequence)

    def add_bbox_to_input(self, x, bbox):
        bbox = torch.transpose(bbox, 1, 2)  # batch_size x 3 x 4
        bbox = bbox[:, :, 0:2]  # batch_size x 4 x 2
        bbox[:, :, 0] *= 128
        bbox[:, :, 1] *= 256
        for b in range(bbox.shape[0]):
            # top left, top right, bottom left, bottom right of bbox
            tl, tr, bl, br = bbox[b].int()
            # Delete everything in other layers that would overlap with bbox
            x[b, :, tl[0]:bl[0], tl[1]:tr[1]] = 0
            # Insert box in right layer.
            x[b, self.label_layer, tl[0]:bl[0], tl[1]:tr[1]] = 1
        return x

    def add_bbox_layer_to_input(self, x, bbox):
        bbox = torch.transpose(bbox, 1, 2)  # batch_size x 3 x 4
        bbox = bbox[:, :, 0:2]  # batch_size x 4 x 2
        bbox[:, :, 0] *= 128
        bbox[:, :, 1] *= 256
        # Allocate new layer to be inserted.
        x_temp = torch.zeros(x.shape[0], 37, 128, 256).to(self.device)
        for b in range(bbox.shape[0]):
            tl, tr, bl, br = bbox[b].int()
            # Delete everything in other layers that would overlap with bbox
            x[b, :, tl[0]:bl[0], tl[1]:tr[1]] = 0
            bbox_layer = torch.zeros(128, 256, device=self.device)
            # Insert new layer.
            bbox_layer[tl[0]:bl[0], tl[1]:tr[1]] = 1
            x_temp[b] = torch.cat((x[b], bbox_layer.unsqueeze(0)), 0)

        return x_temp

    def forward(self, x, a):
        # Define the unitary bonding box to be transformed.
        unitary_bbox = torch.tensor(
            [[0, 0, 1, 1],
             [0, 1, 0, 1],
             [1, 1, 1, 1]],
            device=self.device, dtype=torch.float)

        a_concat = a.view(-1, 2, 3)  # batch_size x 2 x 3
        # Create homogeneous row to be added to a.
        homogen = torch.tensor([0, 0, 1],
                               device=self.device,
                               dtype=torch.float).view(1, 3)  # 1 x 3
        # Repeat the homogeneous row for batch size.
        homogen_cat = homogen.unsqueeze(0).repeat(
            a.shape[0], 1, 1)  # batch_size x 1 x 3

        # Concat a and homogeneous row.
        a_concat = torch.cat((a_concat, homogen_cat), 1)  # batch_size x 3 x 3

        # Transform all unitary boxes with a matrix multiplication with the
        # affine matrix.
        bbox = torch.matmul(a_concat, unitary_bbox)  # batch_size x 3 x 4
        if self.bbox_layer:
            x = self.add_bbox_layer_to_input(x, bbox)
        else:
            x = self.add_bbox_to_input(x, bbox)

        noise = torch.randn(x.shape).mul(self.std).to(self.device)
        # Add instance noise before forwarding.
        x += noise
        return self.model(x).squeeze()


if __name__ == "__main__":
    '''JUST FOR TESTING, DO NOT USE'''

    from torchviz import make_dot

    generator = Generator()
    affine = AffineDiscriminator()
    boxlayout = BoxLayoutDiscriminator()

    models_dict = {
        'generator.encoder': (generator.encoder, (1, 37, 128, 256)),
        'generator.stn': (generator.stn, (1, 64 * 4)),
        'generator.reconstructor': (generator.reconstructor, (1, 256, 1, 1)),
        'generator.common': (generator.common, (1,6)),
        # 'generator.mu': (generator.mu, ()),
        # 'generator.logvar': (generator.logvar, ()),
        'affine.main': (affine.main, (1, 6)),
        'boxlayout.model': (boxlayout.model, (1, 36,128,256))
    }

    for k, v in models_dict.items():
        model = v[0]
        input_shape = v[1]
        name = k
        input = torch.rand(input_shape)
        output = model(input)
        dot = make_dot(output, params=dict(model.named_parameters()))
        dot.format = 'svg'
        dot.render('graphs/' + name)
    # x = torch.rand(64, 36,128,256)
    # print(dot.render())
    # summary(disc.model, input_size=(36,128,256))
    # gen = Generator().to("cuda:0")
    # summary(gen.encoder, input_size=(gen.nc, 128, 256))
    # summary(gen.reconstructor, input_size=(256, 1, 1))

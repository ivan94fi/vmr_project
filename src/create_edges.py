import json
import os

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageDraw
from skimage import data, filters, io
from skimage.draw import polygon_perimeter
from torchvision import transforms
from tqdm import tqdm
import pickle
import sys

"""
This script creates a pkl file with all the ground truth instances edges for
the chosen dataset split.

These edges are used by our data loaders.
"""

if len(sys.argv) < 3:
    print("Usage: create_edges.py [train|val|test] [car|person]")
    sys.exit(1)

split = sys.argv[1]

label = sys.argv[2]  # person or car

path_save = './cityscapes_dataset/{}_{}_edges.pkl'.format(split, label)
root_dir = './cityscapes_dataset/gtFine/{}'.format(split)
json_ext = 'polygons.json'
edges_dict = {}


for _dir in tqdm(os.listdir(root_dir)):
    for _file in tqdm(os.listdir(os.path.join(root_dir, _dir))):
        if _file.split('_')[-1] == json_ext:
            with open(os.path.join(os.path.join(root_dir, _dir, _file))) as f:
                _json = json.load(f)
            edges_list = []
            final_img = np.zeros((128, 256))
            for item in _json['objects']:
                if item['label'] == label:
                    polygon = np.array(item['polygon'])
                    polygon = polygon // 8
                    img = Image.fromarray(np.zeros((128, 256)))
                    img_draw = ImageDraw.Draw(img)
                    img_draw.polygon(list(polygon.ravel()), fill=0, outline=1)
                    final_img = np.logical_or(final_img, np.array(img)).astype(np.float32)

            edges_dict[_file.rsplit('_', 1)[0]] = final_img

with open(path_save, 'wb') as f:
    pickle.dump(edges_dict, f)

# VMR project: re-implementation of the paper *"Context-Aware Synthesis and Placement of Object Instances"*

## Description
This project aims to re-implement a part of the complete pipeline presented in the work of Lee *et al.* [1]. In particular, we re-implemented the *where* module.

## Requirements
* Python 3
* pytorch
* torchvision
* numpy
* matplotlib
* tqdm
* skimage
* tensorboardx
* torchsummary
* imageio

## Execution
* The training procedure is in the file `src/main.py`
* The generation of heatmaps with a pre-trained model is contained in the file `src/test.py`
* The evaluation procedure is in the file `src/evaluate_model_iou.py`
* The files in `fiddle/` are not part of the final project, but served as playground to experiment the newly added features.
* The only exception is the file `fiddle/convert_rgb_to_maps.py`, which is useful to convert the static only (rgb) images to single channel maps.

__NOTE__: every script must be executed in the root directory of the project, adding `src/` or `fiddle/` before its name.
For example, this command would fail:
```
(Starting from the root of the project)
$ cd src
$ python main.py [arguments]
```
and the correct version would be:
```
(Starting from the root of the project)
$ python src/main.py [arguments]
```

### Prerequisites
Before executing the training procedure, it is necessary to perform a pre-processing step, where the bounding boxes and edges of ground truth instances are extracted.
These boxes and edges are then written to some `.pkl` files and finally read during the train loop by our data loader.

In particular it is necessary to execute __both__ the scripts `src/create_bbox.py` and `create_edges.py`.
These scripts take as input the split of the dataset (`train`, `val` or `test`) and the class label for which we want to extract data (either `car` or `person`).
For example, the following command
```
$ python src/create_bbox.py val person
```
would create the bounding box file for all the images in the validation split, for the instances of class `person`.

### Training procedure
The script for the training procedure is `src/main.py` and requires 3 positional arguments.

#### Command line arguments
These arguments are positional and must always be specified.
``` 
    device [cuda:x | cpu]: device where to run the code (see Pytorch docs)
    label [person | car]: objects from cityscapes to utilize
    std [float]: standard deviation of added gaussian instance noise
```
#### Example execution
Run with cuda device with id 1, use person label and apply a gaussian noise with 0.8 stdev to the input of boxlayout discriminator:
```
$ python src/main.py cuda:1 person 0.8
```

### Testing procedure
The script for the testing procedure in in file `src/test.py`. It requires 5 positional arguments.
This script is used to generate heatmaps or boxes and to draw them in the rgb version of cityscapes.
This requires to have a trained generator checkpointed on disk, which will be read and used to perform inference.
By setting `save_on_disk = False` internally, it is possible to send the generated images to tensorboard instead of writing them on disk.

#### Command line arguments
These arguments are positional and must always be specified.
```
    device [cuda:x | cpu]: device where to run the code (see Pytorch docs)
    label [person | car]: objects from cityscapes to utilize
    model: the name of the directory containing the model to restore
    epoch: the epoch at which the model will be restored
    numb_bboxes: how many bounding boxes to generate for each image
```
#### Example execution
Run the testing procedure with cuda device 3, for car instances, using the model checkpointed in directory `car_std08_b64_bbl`, load the model saved at the 15-th epoch,
and generate 300 bounding boxes for each image.

__NOTE__: The base path of the checkpoint directory is specified inside the `main.py` script, which will save models in that directory. This complete base path must also be set inside `test.py`,
and must be the same as the one in `main.py`.

```
$ python src/test.py cuda:3 car car_std08_b64_bbl 15 300
```

### Evaluation procedure
The script for the evaluation procedure in in file `src/evaluate_model_iou.py`. It requires 4 positional arguments.
This script is used to calculate the metrics for every image.
This requires to have a trained generator checkpointed on disk, which will be read and used to perform inference.

__NOTE 1__: the results are printed to stdout so be sure to run the script in a screen, possibly redirecting output to a file.

__NOTE 2__: the experiments are repeated 5 times by default (change the variable `iterations` to something else to change this behavior) and
this can lead to a very slow procedure. See also the source code for the `FIXME`.

#### Command line arguments
These arguments are positional and must always be specified.
```
    device [cuda:x | cpu]: device where to run the code (see Pytorch docs)
    label [person | car]: objects from cityscapes to utilize
    model: the name of the directory containing the model to restore
    epoch: the epoch at which the model will be restored
```
#### Example execution
Run the evaluation procedure with cuda device 3, for car instances, using the model checkpointed in directory `car_std08_b64_bbl`, load the model saved at the 15-th epoch,
and generate 300 bounding boxes for each image. (See `test.py` for further explanations on the directory).
```
$ python src/evaluate_model_iou.py cuda:3 car car_std08_b64_bbl 15
```

## Authors
* **Ivan Prosperi** - <ivan.prosperi@stud.unifi.it>
* **Leonardo Scommegna** - <leonardo.scommegna@stud.unifi.it>

## Bibliography
[1]: D. Lee, S. Liu, J. Gu, M. Liu, M. Yang, and J. Kautz. Context-aware synthesis and placement of object instances. CoRR, abs/1812.02350, 2018.

import torch.nn as nn
from torchsummary import summary

device = "cuda:0"

nc = 37
depth = 64
k = (3, 5)
s = (2, 2)
p = 0
h = 128
w = 256
# inc = 2
d = 1

reconstructor = nn.Sequential(
    # torch.nn.ConvTranspose2d(in_channels, out_channels, kernel_size,
    # stride=1, padding=0, output_padding=0, groups=1, bias=True, dilation=1)

    # nn.ConvTranspose2d( nz, ngf * 8, 4, 1, 0, bias=False),

    nn.ConvTranspose2d((depth * 4), int(depth * 3.5),
                       kernel_size=(8, 16), stride=(1, 1),
                       padding=(0, 0), bias=False, dilation=(1, 1)),
    nn.BatchNorm2d(int(depth * 3.5)),
    nn.ReLU(True),

    nn.ConvTranspose2d(int(depth * 3.5), (depth * 3),
                       kernel_size=(4, 4), stride=(2, 2),
                       padding=(1, 1), bias=False, dilation=(1, 1)),
    nn.BatchNorm2d(depth * 3),
    nn.ReLU(True),

    nn.ConvTranspose2d((depth * 3), (depth * 2),
                       kernel_size=(4, 4), stride=(2, 2),
                       padding=(1, 1), bias=False, dilation=(1, 1)),
    nn.BatchNorm2d(depth * 2),
    nn.ReLU(True),

    nn.ConvTranspose2d((depth * 2), depth,
                       kernel_size=(4, 4), stride=(2, 2),
                       padding=(1, 1), bias=False, dilation=(1, 1)),
    nn.BatchNorm2d(depth),
    nn.ReLU(True),

    nn.ConvTranspose2d(depth, nc,
                       kernel_size=(4, 4), stride=(2, 2),
                       padding=(1, 1), bias=False, dilation=(1, 1)),
    nn.BatchNorm2d(nc),
    nn.ReLU(True),

).to(device)


summary(reconstructor, input_size=(256, 1, 1))


# ******************** #


# encoder = nn.Sequential(
#     # nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False)
#     # input shape: nc * h * w
#     nn.Conv2d(nc, depth, k, s, p, bias=False),
#     nn.LeakyReLU(0.2, inplace=True),
#
#     # shape: (depth) * 64 * 128
#     nn.Conv2d(depth, depth * 2, k, s, p, bias=False),
#     nn.BatchNorm2d(depth * 2),
#     nn.LeakyReLU(0.2, inplace=True),
#
#     # shape: (depth * 2) * 32 * 64
#     nn.Conv2d(depth * 2, depth * 4, k, s, p, bias=False),
#     nn.BatchNorm2d(depth * 4),
#     nn.LeakyReLU(0.2, inplace=True),
#
#     # shape (depth * 4) * 16 * 32
#     nn.AvgPool2d((16, 32))
# ).to(device)

# print(encoder)
# summary(encoder, input_size=(nc, h, w))

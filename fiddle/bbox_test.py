
import matplotlib.pyplot as plt
import imageio
import pickle
import numpy as np
from PIL import Image
from torchvision import transforms

if __name__ == '__main__':
    '''ciao'''
    path_save = '/home/leonardo/workspaces/PycharmProjects/vmr_project/cityscapes_dataset/bbox.pkl'

    img_path = '/home/leonardo/workspaces/PycharmProjects/vmr_project/cityscapes_dataset/gtFine/train/monchengladbach/monchengladbach_000000_018114_gtFine_color.png'
    key = 'monchengladbach_000000_018114_gtFine'

    with open(path_save, 'rb') as f:
        a_dict = pickle.load(f)

    a_list = a_dict[key]
    img = Image.open(img_path)
    transform = transforms.Resize((128, 256), interpolation=Image.NEAREST)
    img = transform(img)
    to_tensor = transforms.ToTensor()
    img = to_tensor(img).permute(1, 2, 0)

    factor = 8
    a = a_list[0]
    mask = np.zeros(img.shape)
    print('a: \n' + str(a))
    a_concat = np.reshape(a, (2, 3)) // factor
    b = np.array([0, 0, 1]).reshape(1, 3)
    a_concat = np.concatenate((a_concat, b))

    print('a_concat: \n' + str(a_concat))

    top_left = np.array([0, 0, 1])
    top_left = np.dot(a_concat, top_left)

    top_right = np.array([0, 1, 1])
    top_right = np.dot(a_concat, top_right)

    bottom_left = np.array([1, 0, 1])
    bottom_left = np.dot(a_concat, bottom_left)

    bottom_right = np.array([1, 1, 1])
    bottom_right = np.dot(a_concat, bottom_right)

    # print(top_left[0], bottom_left[0], top_left[1], top_right[1])
    # print(top_left, top_right, bottom_left, bottom_right)
    # print(img.shape)
    img[top_left[0]:bottom_left[0], top_left[1]:top_right[1], :] = 0
    # top_right = np.dot(a,)
    # print(a[5], a[5]+a[4], a[2], a[2]+a[0])
    # print(top_left[0], bottom_right[0], top_left[1], bottom_right[1])
    # img[a[5]:a[5]+a[4], a[2]:a[2]+a[0], :] = 0
    plt.imshow(img)
    plt.show()

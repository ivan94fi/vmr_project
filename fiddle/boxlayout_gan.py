import sys
import os
from datetime import datetime

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from tqdm import tqdm

from data_reader import DataReader, DataReaderBBox

from models import Generator, BoxLayoutDiscriminator


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Linear') != -1:
        nn.init.xavier_uniform(m.weight)
        m.bias.data.fill_(0.01)
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)

# FIXME: pinned memory


if __name__ == "__main__":

    # ************ PARAMETERS ************ #
    torch.manual_seed(9999)
    train_root = "./cityscapes_dataset/gtFine/train"
    val_root = "./cityscapes_dataset/gtFine/val"
    test_root = "./cityscapes_dataset/gtFine/test"
    train_bbox_path = "./cityscapes_dataset/train_bbox.pkl"
    train_edges_path = "./cityscapes_dataset/train_edges.pkl"
    val_bbox_path = "./cityscapes_dataset/val_bbox.pkl"
    val_edges_path = "./cityscapes_dataset/val_edges.pkl"
    test_bbox_path = "./cityscapes_dataset/test_bbox.pkl"
    test_edges_path = "./cityscapes_dataset/test_edges.pkl"
    model_save_path = "/equilibrium/prosperiscommegna/models"
    epochs = 500
    batch_size = 64  # FIXME prima era 64
    shuffle = True
    num_workers = 4
    resized_h = 128
    resized_w = 256
    scale_factor = 8
    lr = 0.0002  # FIXME provare a mettere uno zero in meno
    beta1 = 0.5
    real_label = 1
    fake_label = 0
    # device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device = torch.device(sys.argv[1])
    datestring = datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M-%S')
    model_save_path = os.path.join(
        "/equilibrium/prosperiscommegna/models/", datestring)
    os.makedirs(model_save_path, exist_ok=True)
    # ************************************ #

    train_bbox_dataset = DataReaderBBox(train_root, train_bbox_path,
                                        train_edges_path,
                                        scale_factor=scale_factor)
    train_bbox_loader = DataLoader(train_bbox_dataset,
                                   batch_size=batch_size,
                                   shuffle=shuffle,
                                   num_workers=num_workers)

    train_dataset = DataReader(train_root, train_edges_path,
                               scale_factor=scale_factor)
    train_loader = DataLoader(train_dataset,
                              batch_size=batch_size,
                              shuffle=shuffle,
                              num_workers=num_workers)

    generator = Generator().to(device)
    # affine_discriminator = AffineDiscriminator().to(device)
    boxlayout_discriminator = BoxLayoutDiscriminator(device=device).to(device)

    # Apply the weights_init function to randomly initialize all weights
    #  to mean=0, stdev=0.2.
    # generator.apply(weights_init)
    # affine_discriminator.apply(weights_init)

    # Initialize loss functions
    adversarial_loss = nn.BCELoss()

    # Initialize optimizers
    # d_affine_optimizer = optim.Adam(
    #     affine_discriminator.parameters(), lr=lr, betas=(beta1, 0.999))
    d_boxlayout_optimizer = optim.Adam(
        boxlayout_discriminator.parameters(), lr=lr, betas=(beta1, 0.999))
    g_optimizer = optim.Adam(
        generator.parameters(), lr=lr, betas=(beta1, 0.999))

    print("Starting main loop")
    ############
    # Main loop
    ############
    for epoch in range(epochs):
        print("epoch [{}/{}]".format(epoch, epochs))

        losses = {}

        data_loaders = {}
        data_loaders['train'] = (train_bbox_loader, train_loader)

        ################
        # Train Loop
        ################

        generator.train()
        boxlayout_discriminator.train()
        # affine_discriminator.train()

        losses['g_adv_layout_loss'] = []
        losses['recon_loss'] = []
        losses['vae_adv_loss'] = []
        losses['final_g_loss'] = []
        losses['d_adv_layout_loss'] = []
        losses['d_adv_sup_layout_loss'] = []
        losses['l1_a'] = []
        losses['kl_div'] = []
        losses['g_adv_sup_layout_loss'] = []

        dataset = train_dataset
        total_iterations = len(dataset) // batch_size + 1

        progress_bar = tqdm(
            enumerate(zip(*data_loaders['train'])),
            total=total_iterations)

        for i, (bbox_data, x) in progress_bar:
            bbox_x, a = bbox_data
            bbox_x = bbox_x.to(device)
            a = a.to(device)
            a[:, :3] /= 128
            a[:, 3:] /= 256
            x = x.to(device)
            current_batch_size = x.shape[0]

            with torch.set_grad_enabled(True):

                real_labels = torch.full((current_batch_size,),
                                         real_label, device=device)

                fake_labels = torch.full((current_batch_size,),
                                         fake_label, device=device)

                #############
                # Train discriminators
                #############

                # affine_discriminator.zero_grad()
                boxlayout_discriminator.zero_grad()

                d_boxlayout_output_real = boxlayout_discriminator(
                        bbox_x, a)
                d_adv_layout_loss_real = adversarial_loss(
                    d_boxlayout_output_real, real_labels)

                d_adv_layout_loss_real.backward()

                z_l = torch.randn(current_batch_size,
                                  resized_h, device=device)
                reconstructed, a_hat, a_tilde, z_a, mu, logvar = generator(
                    x, z_l, a, bbox_x, device)

                d_boxlayout_output_fake = boxlayout_discriminator(
                    x.detach(),
                    a_hat.detach())

                d_adv_layout_loss_fake = adversarial_loss(
                    d_boxlayout_output_fake, fake_labels)

                d_adv_layout_loss_fake.backward()

                d_boxlayout_optimizer.step()
                d_adv_layout_loss = (d_adv_layout_loss_fake
                                     + d_adv_layout_loss_real)

                ############
                # Train generator
                ############

                generator.zero_grad()

                d_boxlayout_output_fake = boxlayout_discriminator(x, a_hat)

                g_adv_layout_loss = adversarial_loss(
                        d_boxlayout_output_fake,
                        real_labels)

                final_g_loss = g_adv_layout_loss
                losses['final_g_loss'].append(final_g_loss.item())

                if True:
                    final_g_loss.backward()
                    g_optimizer.step()

                # End with set_grad_enabled

            # End train / validation loop on data

            # Print losses
            # print("{} losses:".format(phase))
        # print('final_g_loss:\t {:.3f}'.format(final_g_loss.item()))
        print('adv_boxl_loss:\t {:.3f} / {:.3f} (d/g)'.format(
            d_adv_layout_loss.item(), g_adv_layout_loss.item()))
        print('D(x): {:.3f} (affine)'.format(
            d_boxlayout_output_real.item()))
        print('D(G(z)): {:.3f}  (affine)'.format(
            d_boxlayout_output_fake.mean().item()))
        print('')
        # End phase loop

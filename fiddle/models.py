import torch
import torch.nn as nn
from torchsummary import summary
from scipy.misc import imsave
import numpy as np
import pickle


class Flatten(nn.Module):
    def forward(self, x):
        return x.view(x.size()[0], -1)


class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()

        self.nc = 37  # input number of channels
        self.depth = 64  # feature maps base depth
        self.k = 3  # kernel size
        self.s = 2
        self.p = 1

        self.encoder = nn.Sequential(
            # input shape: nc * h * w
            nn.Conv2d(self.nc, self.depth, self.k, self.s, self.p, bias=False),
            nn.LeakyReLU(0.2, inplace=True),

            # shape: (depth) * 64 * 128
            nn.Conv2d(self.depth, self.depth * 2, self.k,
                      self.s, self.p, bias=False),
            nn.BatchNorm2d(self.depth * 2),
            nn.LeakyReLU(0.2, inplace=True),

            # shape: (depth * 2) * 32 * 64
            nn.Conv2d(self.depth * 2, self.depth * 4,
                      self.k, self.s, self.p, bias=False),
            nn.BatchNorm2d(self.depth * 4),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv2d(self.depth * 4, self.depth * 8,
                      self.k, self.s, self.p, bias=False),
            nn.BatchNorm2d(self.depth * 8),
            nn.LeakyReLU(0.2, inplace=True),

            # shape (depth * 4) * 16 * 32
            # nn.AvgPool2d((16, 32))
            Flatten(),
            nn.Linear((self.depth * 8) * 8 * 16, self.depth * 4),

            nn.Tanh()
            # output shape: (depth * 4) * 1 * 1
        )

        self.stn = nn.Sequential(
            # stn: takes the vector e returned from encoder and learns a
            #     transform matrix A (linearized to a vector of 6 elements)
            nn.Linear((self.depth * 4), 4),
            nn.Sigmoid()
            # nn.ReLU(True)
        )

        self.reconstructor = nn.Sequential(

            nn.ConvTranspose2d((self.depth * 4), int(self.depth * 3.5),
                               kernel_size=(8, 16), stride=(1, 1),
                               padding=(0, 0), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(int(self.depth * 3.5)),
            nn.ReLU(True),

            nn.ConvTranspose2d(int(self.depth * 3.5), (self.depth * 3),
                               kernel_size=(4, 4), stride=(2, 2),
                               padding=(1, 1), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(self.depth * 3),
            nn.ReLU(True),

            nn.ConvTranspose2d((self.depth * 3), (self.depth * 2),
                               kernel_size=(4, 4), stride=(2, 2),
                               padding=(1, 1), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(self.depth * 2),
            nn.ReLU(True),

            nn.ConvTranspose2d((self.depth * 2), self.depth,
                               kernel_size=(4, 4), stride=(2, 2),
                               padding=(1, 1), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(self.depth),
            nn.ReLU(True),

            nn.ConvTranspose2d(self.depth, self.nc,
                               kernel_size=(4, 4), stride=(2, 2),
                               padding=(1, 1), bias=False, dilation=(1, 1)),
            nn.BatchNorm2d(self.nc),
            nn.Sigmoid(),
        )

        self.common = nn.Sequential(
            nn.Linear(6, 64),
            nn.ReLU(True)
        )
        self.mu = nn.Linear(64, 128)
        self.logvar = nn.Linear(64, 128)
        # self.mu_2 = nn.Linear(6, 128)
        # self.logvar_2 = nn.Linear(6, 128)

    def unsupervised_path(self, x, z, device):
        z = z.view(-1, 1).repeat(1, 256).view(x.shape[0], 128, -1)
        concat = torch.cat((x, z.unsqueeze(1)), 1)
        e = self.encoder(concat)
        e_unsqueezed = e.view(e.shape[0], e.shape[1], 1, 1)
        reconstructed = self.reconstructor(e_unsqueezed)
        a_hat = self.stn(e)
        # print('a_hat: ', a_hat[0])
        print('a_hat shape:', a_hat.shape)
        a_hat = torch.stack([a_hat[:, 0],
                            torch.zeros(x.shape[0]).to(device),
                            a_hat[:, 1],
                            torch.zeros(x.shape[0]).to(device),
                             a_hat[:, 2],
                            a_hat[:, 3]],
                            dim=1)
        # print('a_hat: ', a_hat[0])
        return reconstructed, a_hat

    def supervised_path(self, a, x_plus, device):
        common = self.common(a)
        mu, logvar = self.mu(common), self.logvar(common)
        # mu, logvar = self.mu_2(a), self.logvar_2(a)
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        z_a = eps.mul(std).add_(mu)
        z_a_concat = z_a.view(-1, 1).repeat(
            1, 256).view(x_plus.shape[0], 128, -1).unsqueeze(1)
        concat = torch.cat((x_plus, z_a_concat), 1)
        a_tilde = self.stn(self.encoder(concat))
        # print('a_tilde: ', a_tilde)
        a_tilde = torch.stack([a_tilde[:, 0],
                               torch.zeros(x_plus.shape[0]).to(device),
                               a_tilde[:, 1],
                               torch.zeros(x_plus.shape[0]).to(device),
                               a_tilde[:, 2],
                               a_tilde[:, 3]],
                              dim=1)
        return a_tilde, z_a, mu, logvar

    def forward(self, x, z, a, x_plus, device):
        reconstructed, a_hat = self.unsupervised_path(x, z, device)
        a_tilde, z_a, mu, logvar = self.supervised_path(a, x_plus, device)
        return reconstructed, a_hat, a_tilde, z_a, mu, logvar


class AffineDiscriminator(nn.Module):
    def __init__(self):
        super(AffineDiscriminator, self).__init__()
        self.main = nn.Sequential(
            nn.Linear(6, 64),
            nn.ReLU(),
            nn.Linear(64, 1),
            nn.Sigmoid()
        )

    def forward(self, a):
        # a[:, :3] *= 128
        # a[:, 3:] *= 256
        # print('prima a: ', a[0])
        # print('a: ', a[0])
        return self.main(a)


class BoxLayoutDiscriminator(nn.Module):
    """Defines a PatchGAN discriminator"""

    def __init__(self, device="cuda:0", ndf=64,
                 n_layers=3, norm_layer=nn.BatchNorm2d):
        """Construct a PatchGAN discriminator
        Parameters:
            input_nc (int)  -- the number of channels in input images
            ndf (int)       -- the number of filters in the last conv layer
            n_layers (int)  -- the number of conv layers in the discriminator
            norm_layer      -- normalization layer
        """
        super(BoxLayoutDiscriminator, self).__init__()
        self.device = device
        use_bias = False
        kw = 4
        padw = 1
        sequence = [nn.Conv2d(36, ndf, kernel_size=kw,
                              stride=2, padding=padw), nn.LeakyReLU(0.2, True)]
        nf_mult = 1
        nf_mult_prev = 1
        for n in range(1, n_layers):  # gradually increase the number of filter
            nf_mult_prev = nf_mult
            nf_mult = min(2 ** n, 8)
            sequence += [
                nn.Conv2d(ndf * nf_mult_prev,
                          ndf * nf_mult,  kernel_size=kw,
                          stride=2, padding=padw, bias=use_bias),
                norm_layer(ndf * nf_mult),
                nn.LeakyReLU(0.2, True)
            ]

        nf_mult_prev = nf_mult
        nf_mult = min(2 ** n_layers, 8)
        sequence += [
            nn.Conv2d(ndf * nf_mult_prev, ndf * nf_mult,
                      kernel_size=kw, stride=1, padding=padw, bias=use_bias),
            norm_layer(ndf * nf_mult),
            nn.LeakyReLU(0.2, True)
        ]
        # output 1 channel prediction map
        sequence += [nn.Conv2d(ndf * nf_mult, 1,
                               kernel_size=kw, stride=1, padding=padw)]
        sequence += [nn.Sigmoid()]
        sequence += [nn.AvgPool2d(kernel_size=(14, 30))]
        self.model = nn.Sequential(*sequence)

    def add_bbox_to_input(self, x, bbox):
        bbox = torch.transpose(bbox, 1, 2)  # 64 x 3 x 4
        bbox = bbox[:, :, 0:2]  # 64 x 4 x 2
        bbox[:, :, 0] *= 128
        bbox[:, :, 1] *= 256
        for b in range(bbox.shape[0]):
            tl, tr, bl, br = bbox[b].int()
            # if tl[0] == bl[0]:
            #     bl[0] += 1
            # if tl[1] == tr[1]:
            #     tr[1] += 1
            x[b, :, tl[0]:bl[0], tl[1]:tr[1]] = 0  # bbox  deleted
            bbox_layer = torch.zeros(128, 256)
            bbox_layer[tl[0]:bl[0], tl[1]:tr[1]] = 1
            x = torch.cat((x, bbox_layer.unsqueeze(0)), 0)
            # x[b, 24, tl[0]:bl[0], tl[1]:tr[1]] = 1
            # mask = np.zeros((128, 256))
            # mask[tl[0]:bl[0], tl[1]:tr[1]] = 1
            # index = np.uint(np.random.rand()*1000)
            # print('index: {}, tlr,blr,tlc,trc {}, {}, {}, {}'
            #       .format(index, tl[0], bl[0], tl[1], tr[1]))
            # scipy.misc.imsave(
            #     '/equilibrium/prosperiscommegna/' + str(index)
            # + '.png', mask)
            # plt.imshow(mask)
            # plt.show()
            return x

    def forward(self, x, a):
        bbox = torch.tensor(
            [[0, 0, 1, 1],
             [0, 1, 0, 1],
             [1, 1, 1, 1]],
            device=self.device, dtype=torch.float)
        a_concat = a.view(-1, 2, 3)  # 64 x 2 x 3
        homogen = torch.tensor([0, 0, 1],
                               device=self.device,
                               dtype=torch.float).view(1, 3)  # 1 x 3
        homogen_cat = homogen.unsqueeze(0).repeat(
            a.shape[0], 1, 1)  # 64 x 1 x 3
        a_concat = torch.cat((a_concat, homogen_cat), 1)  # 64 x 3 x 3
        bbox = torch.matmul(a_concat, bbox)  # 64 x 3 x 4
        # index = np.uint(np.random.rand()*1000)
        # with open('/equilibrium/prosperiscommegna/'
        #           + str(index) + 'prima.pkl', 'wb') as f:
        #     pickle.dump(x.cpu().numpy(), f)
        # with open('/equilibrium/prosperiscommegna/'
        #           + str(index) + 'bbox.pkl', 'wb') as f:
        #     pickle.dump(bbox.detach().cpu().numpy(), f)
        # imsave('/equilibrium/prosperiscommegna/'
        #        + str(index) + 'people_prima.png',
        #        x[0][24].cpu().numpy())
        # imsave('/equilibrium/prosperiscommegna/'
        #        + str(index) + 'edges_prima.png',
        #        x[0][-1].cpu().numpy())

        x = self.add_bbox_to_input(x, bbox)
        # imsave('/equilibrium/prosperiscommegna/'
        #        + str(index) + 'people_dopo.png',
        #        x[0][24].cpu().numpy())
        # imsave('/equilibrium/prosperiscommegna/'
        #        + str(index) + 'edges_dopo.png',
        #        x[0][-1].cpu().numpy())

        # with open('/equilibrium/prosperiscommegna/'
        #           + str(index) + 'dopo.pkl', 'wb') as f:
        #     pickle.dump(x.cpu().numpy(), f)
        return self.model(x).squeeze()


if __name__ == "__main__":
    '''per i test'''
    # gen = Generator().to("cuda:0")
    d = BoxLayoutDiscriminator().to("cuda:0")
    summary(d.model, input_size=(36, 128, 256))
    # summary(gen.encoder, input_size=(gen.nc, 128, 256))
    # summary(gen.reconstructor, input_size=(256, 1, 1))

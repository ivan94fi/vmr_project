from math import floor


def conv(t_in, t_k, t_s, t_p):
    out = tuple()
    for i, k, s, p in zip(t_in, t_k, t_s, t_p):
        o = floor((i + 2 * p - k) / s) + 1
        out += (o,)
    return out


def t_conv(t_in, t_k, t_s, t_p, a):
    out = tuple()
    for i, k, s, p in zip(t_in, t_k, t_s, t_p):
        # print("{}%{}: {}".format((i + 2 * p - k), s, (i + 2 * p - k) % s))
        o = s * (i - 1) + a + k - 2 * p
        # o = i + (k-1)
        out += (o,)

    print(out)
    return out


if __name__ == "__main__":
    i = (1, 1)
    k = (4, 4)
    s0 = (1, 1)
    s = (2, 2)
    p0 = (0, 0)
    p = (1, 1)
    a = 0

    # Funziona ma ha troppi parametri
    # o = t_conv(t_in=(1,1), t_k=(16,32), t_s=(1,1), t_p=(0, 0), a=0)
    # o = t_conv(t_in=o, t_k=(4,4), t_s=(2,2), t_p=(1, 1), a=0)
    # o = t_conv(t_in=o, t_k=(4,4), t_s=(2,2), t_p=(1, 1), a=0)
    # o = t_conv(t_in=o, t_k=(4,4), t_s=(2,2), t_p=(1, 1), a=0)

    o = t_conv(t_in=(1, 1), t_k=(8, 16), t_s=(1, 1), t_p=(0, 0), a=0)
    o = t_conv(t_in=o, t_k=(4, 4), t_s=(2, 2), t_p=(1, 1), a=0)
    o = t_conv(t_in=o, t_k=(4, 4), t_s=(2, 2), t_p=(1, 1), a=0)
    o = t_conv(t_in=o, t_k=(4, 4), t_s=(2, 2), t_p=(1, 1), a=0)
    o = t_conv(t_in=o, t_k=(4, 4), t_s=(2, 2), t_p=(1, 1), a=0)

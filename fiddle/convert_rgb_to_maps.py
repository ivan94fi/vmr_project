import torch
import os
import imageio
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

conversion_dict = {
    7: (128, 64, 128),  # road
    # 1: (0, 0, 0),  # ego vehicle
    8: (244, 35, 232),  # sidewalk
    11: (70, 70, 70),  # building
    # 5: (0, 0, 0),  # dynamic
    21: (107, 142, 35),  # vegetation
    # 4: (0, 0, 0),  # static
    19: (153, 153, 153),  # traffic light
    23: (70, 130, 180),  # sky
    13: (102, 102, 156),  # fence
    # 4: (0, 0, 142),
    # 4: (220, 20, 60),
}

deleted_labels = [24, 25, 26, 27, 28, 32, 33]
# uniques = torch.tensor(
#        [[  0,   0,   0],
#         [  0,   0, 142],
#         [ 70,  70,  70],
#         [ 70, 130, 180],
#         [102, 102, 156],
#         [107, 142,  35],
#         [128,  64, 128],
#         [153, 153, 153],
#         [220,  20,  60],
#         [244,  35, 232]], dtype=torch.uint8)


if __name__ == '__main__':

    base = './cityscapes_dataset'
    # rgb_dir = 'cityscapes500'
    rgb_dir = 'inpainting/val'
    # final_dir = 'cityscapes500_map'
    final_dir = 'cityscapes_inpainting'
    label_dir = 'gtFine'

    uniques_list = []

    for city_dir in os.listdir(os.path.join(base, rgb_dir)):
        print(os.path.join(base, rgb_dir, city_dir))
        for img_name in os.listdir(os.path.join(base, rgb_dir, city_dir)):
            print(img_name)
            rgb_img = imageio.imread(
                os.path.join(base, rgb_dir, city_dir, img_name))

            rgb_img = torch.from_numpy(rgb_img)

            label_img = Image.open(
                os.path.join(base, label_dir, 'val', city_dir, img_name))
            label_img = label_img.resize((256, 128), Image.NEAREST)
            label_img = np.array(label_img)
            label_img = torch.from_numpy(label_img)

            for l in deleted_labels:
                label_img[label_img == l] = 0

            new_img = Image.open(
                os.path.join(base, label_dir, 'val', city_dir, img_name))
            new_img = new_img.resize((256, 128), Image.NEAREST)
            new_img = np.array(label_img)
            new_img = torch.from_numpy(new_img)

            for label, color in conversion_dict.items():
                color = torch.tensor(color, dtype=torch.uint8)
                indices = rgb_img == color
                indices = indices[:,:,0] & indices[:,:,1] & indices[:,:,2]

                if indices.any():
                    new_img[indices] = label

            write_path = os.path.join(base, final_dir, city_dir, img_name)
            imageio.imwrite(write_path, new_img.numpy())

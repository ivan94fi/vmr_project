import imageio
import torch
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle


import json
import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
import torch
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms

np.random.seed(9999)

class DataReaderRGB(Dataset):
    def __init__(self, root_dir, path_bbox):
        self.rgb_ext = "_leftImg8bit.png"
        self.root_dir = root_dir
        self.img_paths = []
        for _dir in os.listdir(self.root_dir):
            for _file in os.listdir(os.path.join(self.root_dir, _dir)):
                self.img_paths.append(os.path.join(self.root_dir,
                                                   _dir,
                                                   _file.rsplit("_", 1)[0]))
        self.img_paths = list(np.unique(self.img_paths))
        with open(path_bbox, 'rb') as f:
            self.a_dict = pickle.load(f)

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, idx):
        # print("key:", idx)
        # print("idx:", idx)
        base_path = self.img_paths[idx]
        print("base:", base_path)
        key = base_path.split("/")[-1] + '_gtFine'
        ll = list(self.a_dict.keys())
        print(ll[0])
        a_list = self.a_dict[key]
        for a in a_list:
            pass

        rgb_path = base_path + self.rgb_ext
        rgb_img = Image.open(rgb_path)

        return rgb_img


if __name__ == "__main__":
    def bbox_to_points(bbox):
        bbox = bbox.T
        stacked = np.column_stack((bbox[:, 0], bbox[:, 1]))
        tl, tr, bl, br = stacked
        return tl, tr, bl, br

    root = "./cityscapes_dataset/leftImg8bit/train"
    path_bbox = './cityscapes_dataset/bbox.pkl'
    dset = DataReaderRGB(root, path_bbox)

    for i, img in enumerate(dset):
        # img = dset[0]
        # plt.imshow(img)
        # plt.show()
        if i == 4:
            break


    # a_concat = np.reshape(a, (2, 3))
    # b = np.array([0, 0, 1]).reshape(1, 3)
    # a_concat = np.concatenate((a_concat, b))
    # print(a_concat)
    # bbox = np.array([[0, 0, 1, 1],
    #                  [0, 1, 0, 1],
    #                  [1, 1, 1, 1]])
    # print('bbox \n', bbox)
    # bbox = np.dot(a_concat, bbox)
    # print('bbox tranformed \n', bbox)
    # top_left, top_right, bottom_left, bottom_right = bbox_to_points(bbox)
    # instance_img = sample[-1, :, :]
    # instance_img[top_left[0]:bottom_left[0], top_left[1]:top_right[1]] = 255000
    # plt.imshow(instance_img)
    # plt.show()


# img_base = "../cityscapes_dataset/gtFine/train/aachen/aachen_000000_000019_gtFine"
#
# color_ext = "_color.png"
# instance_ext = "_instanceIds.png"
# label_ext = "_labelIds.png"
# # polygon_ext = "_polygons.json"
#
# img_color = img_base + color_ext
# img_instance = img_base + instance_ext
# img_label = img_base + label_ext
# img_rgb = "../cityscapes_dataset/leftImg8bit/train/aachen/aachen_000000_000019_leftImg8bit.png"
#
# imgs_paths = [img_color, img_instance, img_label, img_rgb]
# imgs = []
#
# for i_path in imgs_paths:
#     img = imageio.imread(i_path)
#     # print("img.shape", img.shape)
#     # print("img unique values\n", np.unique(img))
#     imgs.append(img)
#     # print("")
#
# def val_shower(im):
#     return lambda x,y: '%d,%d = %d' % (x,y,im[int(y+.5),int(x+.5)])
#
# alpha = 128
# img_rgba = np.dstack([imgs[3], np.full((1024,2048), alpha)])
#
# # img_edges = imgs[0][:,:,3]
# # print(img_edges)
# # print(img_edges.shape)
# # print(np.min(img_edges), np.max(img_edges))
# # print(np.histogram(img_edges))
# # print(np.unique(img_edges))
# plt.subplot(111)
# plt.imshow(imgs[-1]) # , cmap="viridis" Grays, Pastel1
# plt.gca().add_patch(Rectangle((50,100),40,30,linewidth=1,edgecolor='r',facecolor='r'))
#
# # plt.imshow(img_rgba)
# # plt.imshow(imgs[0][:,:,3])
#
# plt.gca().format_coord = val_shower(imgs[1])
#
# plt.show()

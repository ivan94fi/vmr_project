import torch

def make_one_hot(labels, class_num=3):
    '''
    Converts an integer label torch.autograd.Variable to a one-hot Variable.

    Parameters
    ----------
    labels : torch.autograd.Variable of torch.cuda.LongTensor
        N x 1 x H x W, where N is batch size.
        Each value is an integer representing correct classification.
    C : integer.
        number of classes in labels.

    Returns
    -------
    target : torch.autograd.Variable of torch.cuda.FloatTensor
        N x C x H x W, where C is class number. One-hot encoded.
    '''
    _, h, w = labels.size()
    one_hot = torch.FloatTensor(class_num, h, w).zero_()
    # print(labels.scatter_(1, labels.data, 1))
    print("labels: ", labels.shape)
    print("one_hot: ", one_hot.shape)
    target = one_hot.scatter_(0, labels.data, 1)
    return target
    # target = torch.autograd.Variable(labels)
    # return target


if __name__ == "__main__":
    labels = torch.LongTensor(4,4) % 3
    print("initial labels")
    print(labels)
#    labels = labels.unsqueeze(0)
    labels = labels.unsqueeze(0)
    labels = make_one_hot(labels)
    print("*" * 80)
    print("one-hot labels")
    print(labels)

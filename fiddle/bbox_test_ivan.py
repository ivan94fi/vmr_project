import pickle

import imageio
import matplotlib.pyplot as plt
import numpy as np

path_save = '../cityscapes_dataset/bbox.pkl'
key = 'krefeld_000000_023698_gtFine'
img_path = '../cityscapes_dataset/gtFine/train/krefeld/krefeld_000000_023698_gtFine_color.png'


def bbox_to_transform(arr):
    arr = np.array(arr)
    assert(arr.shape == (6,))
    last_row = np.array([0, 0, 1]).reshape(1, 3)
    mat = np.concatenate([arr.reshape(2, 3), last_row])
    return mat


def bbox_to_points(bbox):
    bbox = bbox.T
    stacked = np.column_stack((bbox[:, 0], bbox[:, 1]))
    tl, tr, bl, br = stacked
    return tl, tr, bl, br


with open(path_save, 'rb') as f:
    bboxes = pickle.load(f)


# print(bboxes.keys())
a = bboxes[key][0]
print(a)
a = bbox_to_transform(a)
print(a)

bbox = np.array(
    [[0, 0, 1, 1],
     [0, 1, 0, 1],
     [1, 1, 1, 1]]
)
print(bbox)

tr_bbox = np.dot(a, bbox)
print(tr_bbox)

tl, tr, bl, br = bbox_to_points(tr_bbox)
print(tl, tr, bl, br)

img = imageio.imread(img_path)
img[tl[0]:bl[0], tl[1]:tr[1]] = 255
plt.imshow(img)
plt.show()

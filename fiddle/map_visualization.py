import imageio
import torch
import numpy as np
import matplotlib.pyplot as plt

img_base = "../cityscapes_dataset/gtFine/train/aachen/aachen_000000_000019_gtFine"

color_ext = "_color.png"
instance_ext = "_instanceIds.png"
label_ext = "_labelIds.png"
# polygon_ext = "_polygons.json"

img_color = img_base + color_ext
img_instance = img_base + instance_ext
img_label = img_base + label_ext
img_rgb = "../cityscapes_dataset/leftImg8bit/train/aachen/aachen_000000_000019_leftImg8bit.png"

imgs_paths = [img_color, img_instance, img_label, img_rgb]
imgs = []

for i_path in imgs_paths:
    img = imageio.imread(i_path)
    # print("img.shape", img.shape)
    # print("img unique values\n", np.unique(img))
    imgs.append(img)
    # print("")

def val_shower(im):
    return lambda x,y: '%d,%d = %d' % (x,y,im[int(y+.5),int(x+.5)])

alpha = 128
img_rgba = np.dstack([imgs[3], np.full((1024,2048), alpha)])

img_edges = imgs[0][:,:,3]
print(img_edges)
print(img_edges.shape)
print(np.min(img_edges), np.max(img_edges))
print(np.histogram(img_edges))
print(np.unique(img_edges))
plt.subplot(111)
# plt.imshow(imgs[1], cmap="viridis") # Grays, Pastel1
# plt.imshow(img_rgba)
# plt.imshow(imgs[0][:,:,3])

plt.gca().format_coord = val_shower(imgs[1])

plt.show()


